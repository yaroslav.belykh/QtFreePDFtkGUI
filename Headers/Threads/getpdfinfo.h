/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef GETPDFINFO_H
#define GETPDFINFO_H

#include <QObject>
#include <QSettings>

#include "Headers/Acctions/pdfinfo.h"
#include "Headers/Acctions/mimetype.h"
#include "Headers/Structures/pdfinfo.h"

class getPDFInfo : public QObject
{
    Q_OBJECT
public:
    explicit getPDFInfo(QSettings *, QStringList, QObject *parent = 0);

signals:
    void currentFileInfo(QString, QString, int, QDateTime, QString, QString, QString);
    void modelIsUpdated(void);
    void allDone();
public slots:
    void doWork();
private:
    PdfInfo *infoPDF;
    mimeType *MimeType;

    QSettings *settings;
    QStringList pdfFiles;

};

#endif // GETPDFINFO_H
