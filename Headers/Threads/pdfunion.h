/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef PDFUNION_H
#define PDFUNION_H

#include <QObject>
#include <QDebug>
#include <QFile>
#include <QDir>
#include <QSettings>
#include <QProcess>

//#include "Headers/Structures/sql.h"
#include "Headers/Acctions/makeonepdf.h"
#include "Headers/Acctions/mimetype.h"

#include "Headers/Structures/pdfinfo.h"

class pdfUnion : public QObject
{
    Q_OBJECT
public:
    //explicit pdfUnion(QObject *parent = 0);
    pdfUnion(QSettings*, TPdfFileList, QObject *parent = 0);
    ~pdfUnion();

signals:
    void setUnionPDFStatus(QString, int, QString);
    void finished(void);

public slots:
    void doWork();

private:
    TPdfFileList workList;

    CrcHash *Crc;

    mimeType *MimeType;

    QSettings *settings;

    inline QString getFilePath(QString);
};

#endif // PDFUNION_H
