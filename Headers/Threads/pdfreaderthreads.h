/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef PDFREADERTHREADS_H
#define PDFREADERTHREADS_H

#include <QObject>
#include <QImage>
#include <QLabel>
#include <QPainter>

#include <poppler-qt5.h>

#include "Headers/Structures/common.h"
#include "Headers/Structures/pdfimagestructures.h"
#include "Headers/Widgets/wgxgraphics.h"

class PdfReaderThreads : public QObject
{
    Q_OBJECT
public:
    explicit PdfReaderThreads(QString *, int inpDPI = 300,  QObject *parent = nullptr);

    int getPageNumbers(void);

public slots:
    void doPdf2ImageRender();

signals:
    void setPage(int, QImage, QSize, int);
    void getPageCount(int);
    void allDone();


private:
    QString *pdfFilePath;

    Poppler::Document *pdfFile;
    int dpi;

    QMap<int, TImagePage> mImage;
    QMap<int, wgxGraphics*> GraphicsWGX;
    QMap<int, QLabel*> mLabel;
};

#endif // PDFREADERTHREADS_H
