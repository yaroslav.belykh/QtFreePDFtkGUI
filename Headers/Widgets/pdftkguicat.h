/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef PDFTKGUICAT_H
#define PDFTKGUICAT_H

#include <QWidget>
#include <QSettings>
#include <QFileDialog>
#include <QThread>
#include <QItemSelectionModel>
#include <QMessageBox>

#include "Headers/Threads/pdfunion.h"
#include "Headers/Threads/getpdfinfo.h"

#include "Headers/Models/pdflist.h"

#include "Headers/Widgets/pdfwiewform.h"

#include "Headers/Widgets/addexistfileagain.h"

namespace Ui {
class PdfTkGuiCat;
}

class PdfTkGuiCat : public QWidget
{
    Q_OBJECT

public:
    explicit PdfTkGuiCat(QSettings*, QWidget *parent = 0);
    ~PdfTkGuiCat();

public slots:
    void restore_collumn_width(void);

    void getExistFile(bool, int);

    void getSelectedPagesList(QString);

private slots:
    void currentFileInfo(QString, QString, int, QDateTime, QString, QString, QString);
    void allInfoDone();
    void pdfUnionDone(QString, int, QString);
    void pdfUnionDelete();

    void on_pbAdd_clicked();

    void on_pbChoose_clicked();

    void on_pbUp_clicked();

    void on_pbDown_clicked();

    void on_pbDelete_clicked();

    void on_pdDoIt_clicked();

    void on_pbAddSelected_clicked();

    void on_tableView_doubleClicked(const QModelIndex &index);

    void on_tableView_clicked(const QModelIndex &index);

private:
    Ui::PdfTkGuiCat *ui;

    getPDFInfo *PdfInfo;
    pdfUnion *UnionPDF;
    PDFWiewForm *formPDF;

    AddExistFileAgain *getExistFileDiaog;

    PDFList *pdfListModel;
    QSortFilterProxyModel *SortModel;
    QItemSelectionModel *ItemSelectionModel;

    QSettings *settings;
    QThread *Thread;

    QStringList inputFiles;
    QString outputFile;

    int selectedPDF(void);

    inline void deleteViewPDF(void);
    int lastID;
};

#endif // PDFTKGUICAT_H
