/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef PDFTKGUIBOOKMARKS_H
#define PDFTKGUIBOOKMARKS_H

#include <QWidget>
#include <QSettings>
#include <QTemporaryDir>
#include <QFileDialog>
#include <QThread>
#include <QItemSelectionModel>
#include <QMessageBox>

#include "Headers/Models/bookmarkslist.h"
#include "Headers/Threads/getmetainfo.h"
#include "Headers/Threads/metadatawriter.h"

#include "Headers/Widgets/bookmarksedit.h"

namespace Ui {
class PdfTkGUiBookmarks;
}

class PdfTkGUiBookmarks : public QWidget
{
    Q_OBJECT

public:
    explicit PdfTkGUiBookmarks(QSettings*, QTemporaryDir*, QWidget *parent = 0);
    ~PdfTkGUiBookmarks();

    int selectedBookmarks(void);

public slots:
    void allInfoDone();
    void allAddInfoDone();
    void restore_collumn_width(void);
    void editDialogOK(bool, int, int, QString, int);
    void getStaus(QString, int);
   void writeDone();

private slots:
    void on_pbChoose_InPut_clicked();

    void on_pbChooseOutput_clicked();

    void on_pbAdd_clicked();

    void on_pushButton_clicked();

    void on_pbEdit_clicked();

    void on_pbDelete_clicked();

    void on_tableView_clicked(const QModelIndex &index);

    void on_tableView_doubleClicked(const QModelIndex &index);

    void on_pbInsert_clicked();

    void on_pbAddSelected_clicked();

private:
    Ui::PdfTkGUiBookmarks *ui;

    QSettings *settings;
    QThread *Thread;
    QTemporaryDir *tmpDir;

    BookMarksEdit *EditDialog;

    getMetaInfo *MetaInfo;

    BookmarksList *bookmarksListModel;
    QSortFilterProxyModel *SortModel;
    QItemSelectionModel *ItemSelectionModel;

    QString inputFile;
    QString outputFile;

    QStringList Data;
    QStringList *aData;

    int PageNumber;
    int LastAPages;

    int curID;

    int bmStringStart;
    int bmStringStop;

    MetaDataWriter *MetaWriter;

    inline void setEnableButtons(bool);
    inline void setEnabledEditButtons(bool);
};

#endif // PDFTKGUIBOOKMARKS_H
