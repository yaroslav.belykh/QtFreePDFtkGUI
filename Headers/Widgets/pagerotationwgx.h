/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef PAGEROTATIONWGX_H
#define PAGEROTATIONWGX_H

#include <QWidget>

namespace Ui {
class PageRotationWgx;
}

class PageRotationWgx : public QWidget
{
    Q_OBJECT

public:
    explicit PageRotationWgx(QWidget *parent = 0);
    ~PageRotationWgx();

    void enableOrientation(int);
    void enableImage(bool);

    int Value(void);
    bool Status(void);

signals:
    void setStatus();

private slots:
    void on_tbOrig_toggled(bool checked);

    void on_tbEast_toggled(bool checked);

    void on_tbSouth_toggled(bool checked);

    void on_tbWest_toggled(bool checked);

    void on_checkBox_toggled(bool checked);

private:
    Ui::PageRotationWgx *ui;

    int Data;

    bool Checked;

    int lastValue;

    void setValue(int, bool);
};

#endif // PAGEROTATIONWGX_H
