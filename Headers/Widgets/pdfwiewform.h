/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef PDFWIEWFORM_H
#define PDFWIEWFORM_H

#include <QWidget>
#include <QSettings>
#include <QLabel>
#include <QToolBar>
#include <QHBoxLayout>
#include <QThread>
#include <QScrollBar>
#include <QCheckBox>

#include "Headers/Threads/pdfreaderthreads.h"
#include "Headers/Widgets/windowprogressbar.h"
#include "Headers/Widgets/pagerotationwgx.h"
#include "Headers/Structures/pdfimagestructures.h"

namespace Ui {
class PDFWiewForm;
}

class PDFWiewForm : public QWidget
{
    Q_OBJECT

public:
    explicit PDFWiewForm(QSettings *inpSettings, QString inpFilePath, QString inpSelectedPages, QWidget *parent = 0, int dpiV = 300, int dpiP = 300);
    ~PDFWiewForm();

    void setRotate(int, int);

signals:
    void setPages(QString);

private slots:
    void setScaleChange(QString);
    void getPage(int, QImage, QSize, int);
    void getPageCount(int);
    void getPageProgreeNumber(int);
    void getPagePrepareComplete(bool, int);
    void openedEnd(void);
    void rechoosePages(void);

private:
    Ui::PDFWiewForm *ui;

    PdfReaderThreads *ReaderPDF;
    WindowProgressBar *ProgressBar;

    QThread *Thread;
    QSettings *settings;

    int printDPI;
    int viewDPI;
    QString ipf;

    QMap<int, TImagePage> mImage;
    QMap<int, wgxGraphics*> GraphicsWGX;
    QMap<int, QLabel*> mLabel;
    QMap<int, QCheckBox*> mCheckBox;
    QMap<int, PageRotationWgx*> mPageRotation;
    QMap<int, QHBoxLayout*> Hboxes;

    QList<int> intSelectedPages;
    QMap<int, int> intRotationPage;

    inline QList<int> makeSelectedPagesList(QString);
    inline QMap<int, int> makeRotationPageList(QString);

    bool checkSelectedList(int);
    int orientationSelectList(int);
};

#endif // PDFWIEWFORM_H
