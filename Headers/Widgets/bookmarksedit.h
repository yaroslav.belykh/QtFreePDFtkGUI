/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef BOOKMARKSEDIT_H
#define BOOKMARKSEDIT_H

#include <QDialog>
#include <QAbstractButton>

namespace Ui {
class BookMarksEdit;
}

class BookMarksEdit : public QDialog
{
    Q_OBJECT

public:
    explicit BookMarksEdit(int pageMax = 9999, int levelMax = 10, int pageMin = 1, int levelMin = 1, int Page = 1, int Level = 1, QString Tittle = "", int typeDialog = 0, QWidget *parent = 0);
    ~BookMarksEdit();

signals:
    void setData(bool, int, int, QString, int);

private slots:
    void on_buttonBox_clicked(QAbstractButton *button);

private:
    Ui::BookMarksEdit *ui;

    int typeOperation;
};

#endif // BOOKMARKSEDIT_H
