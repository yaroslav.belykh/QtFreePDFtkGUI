/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef WINDOWPROGRESSBAR_H
#define WINDOWPROGRESSBAR_H

#include <QWidget>

namespace Ui {
class WindowProgressBar;
}

class WindowProgressBar : public QWidget
{
    Q_OBJECT

public:
    explicit WindowProgressBar(QString winTitle, QWidget *parent = 0, Qt::WindowModality Modal = Qt::WindowModal);
    ~WindowProgressBar();

    void setMaximum(int Maximum);
    void setValue(int Value);

private:
    Ui::WindowProgressBar *ui;
};

#endif // WINDOWPROGRESSBAR_H
