/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef BELYKHBASEFUNCTIONS_H
#define BELYKHBASEFUNCTIONS_H

#include <QObject>
#include <QList>
#include <QStringList>
#include <QtCore/qmath.h>
#include <QVariant>
#include <QTime>
#include <QAbstractEventDispatcher>
#include <QCoreApplication>
#include <QDir>
#include <QByteArray>
#include <QBitArray>

class belykhBaseFunctions : public QObject
{
    Q_OBJECT
public:
    explicit belykhBaseFunctions(QObject *parent = 0);
    ~belykhBaseFunctions();

    QStringList grep(QStringList, QString, bool xst = true);
    QString awk(QString, QString, int);
    QString StrList2Str(QStringList, QString sep = " ", QString Scrng = "");
    QString firstSymbolToUpper(QString);
    QString AllFirstSymbolToUpper(QString, QString);
    QString roundString(QString, QString, int roundPosit = 3);
    double roundTo(double, int inpCount = 0);
    QBitArray bytesToBits(const QByteArray &bytes);
    QByteArray bitsToBytes(const QBitArray &bits);

    QString replaceSubString(QString, QString, QString);
    QString restorePath(QString, bool isUnix = true);
    QString replacePath2Cygwin(QString);

signals:

public slots:
};

#endif // BELYKHBASEFUNCTIONS_H
