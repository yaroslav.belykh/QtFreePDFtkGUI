/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef PDFINFOSTRUCTURES_H
#define PDFINFOSTRUCTURES_H

#include <QString>
#include <QMap>
#include <QDateTime>

typedef struct vPdfFileList
{
    QStringList inputFiles;
    QMap<int, QStringList> inputPages;
    QString outputFile;
    int crcAlg;
}TPdfFileList;

typedef struct vPdfBookMarks
{
    QString Title;
    int Level;
    int PageNumber;
}TpdfBookMarks;

typedef struct vPdfPagesMedia
{
    int pWidth;
    int pHeight;
    int Orientation;
}TpdfPagesMedia;

typedef struct vPdfInfo
{
    QString fileName;
    QDateTime ModDate;
    QDateTime CreationDate;
    QString Creator;
    QString Producer;
    QString Author;
    QString Title;
    int Pages;
    QString mimeType;
    QString selectedPages;
    bool isMakeList;
    QString fileID;
}TpdfInfo;

typedef struct vPdfInfoFull
{
    TpdfInfo mainInfo;
    QMap<int, QString> PdfID;
    QMap<int, TpdfBookMarks> BookMarks;
    QMap<int, TpdfPagesMedia> PageMedia;
}TpdfInfoFull;



#endif // PDFINFOSTRUCTURES_H
