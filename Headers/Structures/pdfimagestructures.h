/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef PDFIMAGESTRUCTURES_H
#define PDFIMAGESTRUCTURES_H
#include <QImage>
#include <QPainter>
#include <QSize>

#include "Headers/Structures/common.h"

typedef struct vImagePage
{
    QImage Image;
    int Orientation;
    QSize Size;
}TImagePage;

#endif // PDFIMAGESTRUCTURES_H
