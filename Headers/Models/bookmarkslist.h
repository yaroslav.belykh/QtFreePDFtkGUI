/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef BOOKMARKSLIST_H
#define BOOKMARKSLIST_H

#include <QObject>
#include <QAbstractTableModel>
#include <QSortFilterProxyModel>
#include <QModelIndex>
#include <QVariant>
#include "Headers/Structures/pdfinfo.h"

class BookmarksList : public QAbstractTableModel
{
    Q_OBJECT
public:
    BookmarksList(QWidget *parent = 0);
    ~BookmarksList();

    void updateModel(TpdfBookMarks, int indexRow = 0, int updateType = 0);
    void clearData();

    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    TpdfBookMarks getPdfMetaData(int);

signals:

public slots:

private:
    QMap<int,TpdfBookMarks> pdfBookmarks;

    int columnCount(const QModelIndex &parent) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    QVariant data(const QModelIndex &index, int role) const;
};

#endif // BOOKMARKSLIST_H
