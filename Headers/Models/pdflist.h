/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef PDFLIST_H
#define PDFLIST_H

#include <QObject>
#include <QAbstractTableModel>
#include <QSortFilterProxyModel>
#include <QModelIndex>
#include <QVariant>

#include "Headers/Structures/pdfinfo.h"

class PDFList : public QAbstractTableModel
{
public:
    PDFList(QWidget *parent = 0);
    ~PDFList();

    void updateModel(TpdfInfo, int indexRow = 0, int updateType = 0);
    void clearData();

    int charID;

    QStringList getAllFiles(void);
    QMap<int, QStringList> getAllPages(void);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    QStringList filesList(void);
    QString getFileName(int);
    QString getSelectedPages(int);
    void setSelectedPages(int, QString);

    TpdfInfo getCurrentFileINfo(int);

private:
    QMap<int,TpdfInfo> pdfFiles;

    int columnCount(const QModelIndex &parent) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    QVariant data(const QModelIndex &index, int role) const;

};

#endif // PDFLIST_H
