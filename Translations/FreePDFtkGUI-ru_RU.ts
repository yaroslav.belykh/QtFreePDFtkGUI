<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../Forms/About/aboutdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Forms/About/aboutdialog.ui" line="44"/>
        <source>About</source>
        <translation type="unfinished">О программе</translation>
    </message>
    <message>
        <location filename="../Forms/About/aboutdialog.ui" line="54"/>
        <source>Developers</source>
        <translation type="unfinished">Разработчик</translation>
    </message>
    <message>
        <location filename="../Forms/About/aboutdialog.ui" line="82"/>
        <source>Close</source>
        <translation type="unfinished">Закрыть</translation>
    </message>
</context>
<context>
    <name>AddExistFileAgain</name>
    <message>
        <location filename="../Forms/Widgets/addexistfileagain.ui" line="26"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../Forms/mainwindow.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="28"/>
        <source>Union PDF</source>
        <translation type="unfinished">Объединить PDF</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="34"/>
        <source>PDF Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="53"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;Файл</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="60"/>
        <source>&amp;Settings</source>
        <translation type="unfinished">&amp;Настройки</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="66"/>
        <source>&amp;Help</source>
        <translation type="unfinished">&amp;Помощь</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="86"/>
        <source>&amp;About Qt</source>
        <translation type="unfinished">О &amp;Qt</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="91"/>
        <source>A&amp;bout</source>
        <translation type="unfinished">О &amp;программе</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="96"/>
        <source>&amp;Exit</source>
        <translation type="unfinished">&amp;Выход</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="101"/>
        <source>&amp;Program Path&apos;s</source>
        <translation type="unfinished">Програ&amp;ммные пути</translation>
    </message>
    <message>
        <location filename="../Forms/mainwindow.ui" line="106"/>
        <source>&amp;New</source>
        <translation type="unfinished">&amp;Новый</translation>
    </message>
    <message>
        <location filename="../Source/mainwindow.cpp" line="40"/>
        <source>Executable Files ( *.exe );; All Files ( *.* )</source>
        <translation type="unfinished">Исполняемые файлы ( *.exe );; Все файлы ( *.* )</translation>
    </message>
    <message>
        <location filename="../Source/mainwindow.cpp" line="43"/>
        <location filename="../Source/mainwindow.cpp" line="46"/>
        <source>All Files ( * )</source>
        <translation type="unfinished">Все файлы ( * )</translation>
    </message>
    <message>
        <location filename="../Source/mainwindow.cpp" line="48"/>
        <source>Please chose PdfTk executable file!</source>
        <translation type="unfinished">Пожалуйста, выберите исполняемый файл PdfTk</translation>
    </message>
</context>
<context>
    <name>PDFList</name>
    <message>
        <location filename="../Source/Models/pdflist.cpp" line="163"/>
        <source>№</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Source/Models/pdflist.cpp" line="166"/>
        <source>File name</source>
        <translation type="unfinished">Имф файлы</translation>
    </message>
    <message>
        <location filename="../Source/Models/pdflist.cpp" line="169"/>
        <source>File type</source>
        <translation type="unfinished">Тип файла</translation>
    </message>
    <message>
        <location filename="../Source/Models/pdflist.cpp" line="172"/>
        <source>Pages</source>
        <translation type="unfinished">Страницы</translation>
    </message>
    <message>
        <location filename="../Source/Models/pdflist.cpp" line="175"/>
        <source>Creation date</source>
        <translation type="unfinished">Дата создания</translation>
    </message>
    <message>
        <location filename="../Source/Models/pdflist.cpp" line="178"/>
        <source>Title</source>
        <translation type="unfinished">Название</translation>
    </message>
    <message>
        <location filename="../Source/Models/pdflist.cpp" line="181"/>
        <source>Author</source>
        <translation type="unfinished">Автор</translation>
    </message>
    <message>
        <location filename="../Source/Models/pdflist.cpp" line="184"/>
        <source>Creator</source>
        <translation type="unfinished">Создатель</translation>
    </message>
    <message>
        <location filename="../Source/Models/pdflist.cpp" line="187"/>
        <source>Selected Pages</source>
        <translation type="unfinished">Выбранные страницы</translation>
    </message>
    <message>
        <location filename="../Source/Models/pdflist.cpp" line="190"/>
        <source>FileID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Source/Models/pdflist.cpp" line="238"/>
        <source>All</source>
        <translation type="unfinished">Все</translation>
    </message>
</context>
<context>
    <name>PDFWiewForm</name>
    <message>
        <location filename="../Forms/Widgets/pdfwiewform.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Source/Widgets/pdfwiewform.cpp" line="24"/>
        <source>Opening PDF file</source>
        <translation type="unfinished">Открыть PDF файл</translation>
    </message>
</context>
<context>
    <name>PdfInfo</name>
    <message>
        <location filename="../Source/Acctions/pdfinfo.cpp" line="29"/>
        <source>Read file</source>
        <translation type="unfinished">Чтение файла</translation>
    </message>
</context>
<context>
    <name>PdfTkGUiBookmarks</name>
    <message>
        <location filename="../Forms/Widgets/pdftkguibookmarks.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Forms/Widgets/pdftkguibookmarks.ui" line="28"/>
        <source>Input File:</source>
        <translation type="unfinished">Входящий файл:</translation>
    </message>
    <message>
        <location filename="../Forms/Widgets/pdftkguibookmarks.ui" line="38"/>
        <location filename="../Forms/Widgets/pdftkguibookmarks.ui" line="152"/>
        <source>Choose</source>
        <translation type="unfinished">Выбрать</translation>
    </message>
    <message>
        <location filename="../Forms/Widgets/pdftkguibookmarks.ui" line="57"/>
        <source>Add bookmark</source>
        <translation type="unfinished">Добавить закладку</translation>
    </message>
    <message>
        <location filename="../Forms/Widgets/pdftkguibookmarks.ui" line="70"/>
        <source>Add bookmarks from file</source>
        <translation type="unfinished">добавить заклдаки из файла</translation>
    </message>
    <message>
        <location filename="../Forms/Widgets/pdftkguibookmarks.ui" line="77"/>
        <source>Edit bookmark</source>
        <translation type="unfinished">Редактировать закладку</translation>
    </message>
    <message>
        <location filename="../Forms/Widgets/pdftkguibookmarks.ui" line="90"/>
        <source>Delete bookmarks</source>
        <translation type="unfinished">Удалить закладки</translation>
    </message>
    <message>
        <location filename="../Forms/Widgets/pdftkguibookmarks.ui" line="103"/>
        <source>Up</source>
        <translation type="unfinished">Вверх</translation>
    </message>
    <message>
        <location filename="../Forms/Widgets/pdftkguibookmarks.ui" line="116"/>
        <source>Down</source>
        <translation type="unfinished">Вниз</translation>
    </message>
    <message>
        <location filename="../Forms/Widgets/pdftkguibookmarks.ui" line="142"/>
        <source>Output File:</source>
        <translation type="unfinished">Выходной файл</translation>
    </message>
    <message>
        <location filename="../Forms/Widgets/pdftkguibookmarks.ui" line="161"/>
        <source>Save Bookmarks in PDF</source>
        <translation type="unfinished">Сохранить заклдаки в PDF-файл</translation>
    </message>
</context>
<context>
    <name>PdfTkGuiCat</name>
    <message>
        <location filename="../Forms/Widgets/pdftkguicat.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Forms/Widgets/pdftkguicat.ui" line="23"/>
        <source>File list</source>
        <translation type="unfinished">Список файлов</translation>
    </message>
    <message>
        <location filename="../Forms/Widgets/pdftkguicat.ui" line="40"/>
        <source>Add files</source>
        <translation type="unfinished">Добавить файл</translation>
    </message>
    <message>
        <location filename="../Forms/Widgets/pdftkguicat.ui" line="53"/>
        <source>Add exist file again</source>
        <translation type="unfinished">Добавить еще раз файл</translation>
    </message>
    <message>
        <location filename="../Forms/Widgets/pdftkguicat.ui" line="66"/>
        <source>Delete files</source>
        <translation type="unfinished">Удалить файл</translation>
    </message>
    <message>
        <location filename="../Forms/Widgets/pdftkguicat.ui" line="79"/>
        <source>Up</source>
        <translation type="unfinished">Вверх</translation>
    </message>
    <message>
        <location filename="../Forms/Widgets/pdftkguicat.ui" line="92"/>
        <source>Down</source>
        <translation type="unfinished">Вниз</translation>
    </message>
    <message>
        <location filename="../Forms/Widgets/pdftkguicat.ui" line="110"/>
        <source>Outut file</source>
        <translation type="unfinished">Выходной файл</translation>
    </message>
    <message>
        <location filename="../Forms/Widgets/pdftkguicat.ui" line="122"/>
        <source>Choose</source>
        <translation type="unfinished">Выбрать</translation>
    </message>
    <message>
        <location filename="../Forms/Widgets/pdftkguicat.ui" line="131"/>
        <source>Union PDF</source>
        <translation type="unfinished">Объединить PDF</translation>
    </message>
    <message>
        <location filename="../Source/Widgets/pdftkguicat.cpp" line="123"/>
        <source>Another error!</source>
        <translation type="unfinished">Другая ошибка!</translation>
    </message>
    <message>
        <location filename="../Source/Widgets/pdftkguicat.cpp" line="138"/>
        <source>Files to add union PDF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../Source/Widgets/pdftkguicat.cpp" line="138"/>
        <location filename="../Source/Widgets/pdftkguicat.cpp" line="158"/>
        <source>PDF Files ( *.pdf );; All Files ( *.* )</source>
        <translation type="unfinished">PDF файлы ( *.pdf );; Все файлы ( *.* )</translation>
    </message>
    <message>
        <location filename="../Source/Widgets/pdftkguicat.cpp" line="158"/>
        <source>File to save union PDF</source>
        <translation type="unfinished">Файл для сохранения объеденненого PDF</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../Source/Widgets/pdftkguicat.cpp" line="114"/>
        <source>File was created</source>
        <translation type="unfinished">Файл создан</translation>
    </message>
    <message>
        <location filename="../Source/Widgets/pdftkguicat.cpp" line="117"/>
        <source>File was not fonded!</source>
        <translation type="unfinished">Файл не найден!</translation>
    </message>
    <message>
        <location filename="../Source/Widgets/pdftkguicat.cpp" line="120"/>
        <source>File is not PDF</source>
        <translation type="unfinished">Это не PDF файл</translation>
    </message>
    <message>
        <location filename="../Source/Widgets/pdftkguicat.cpp" line="123"/>
        <source>Error!</source>
        <translation type="unfinished">Ошибка!</translation>
    </message>
</context>
<context>
    <name>WindowProgressBar</name>
    <message>
        <location filename="../Forms/Widgets/windowprogressbar.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>pdfUnion</name>
    <message>
        <location filename="../Source/Threads/pdfunion.cpp" line="78"/>
        <source>MIME type is not PDF: </source>
        <translation type="unfinished">MIME-Тип файла не PDF: </translation>
    </message>
    <message>
        <location filename="../Source/Threads/pdfunion.cpp" line="81"/>
        <location filename="../Source/Threads/pdfunion.cpp" line="109"/>
        <source>File not founded: </source>
        <translation type="unfinished">Файл не найден: </translation>
    </message>
</context>
<context>
    <name>wgxGraphics</name>
    <message>
        <location filename="../Forms/Widgets/wgxgraphics.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
