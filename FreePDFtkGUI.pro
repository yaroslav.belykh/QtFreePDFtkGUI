#-------------------------------------------------
#
# Project created by QtCreator 2017-07-24T09:04:16
#
#-------------------------------------------------

QT       += core gui testlib printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FreePDFtkGUI
TEMPLATE = app

# CONFIG += c++11
# CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += Source/main.cpp\
        Source/mainwindow.cpp \
        Source/About/aboutdialog.cpp \
        Source/Commons/basefunctions.cpp \
        Source/Acctions/crchash.cpp \
        Source/Acctions/mimetype.cpp \
        Source/Acctions/makeonepdf.cpp \
        Source/Acctions/pdfinfo.cpp \
        Source/Threads/pdfunion.cpp \
    Source/Widgets/pdftkguicat.cpp \
    Source/Models/pdflist.cpp \
    Source/Threads/getpdfinfo.cpp \
    Source/Widgets/addexistfileagain.cpp \
    Source/Widgets/pdfwiewform.cpp \
    Source/Threads/pdfreaderthreads.cpp \
    Source/Widgets/windowprogressbar.cpp \
    Source/Widgets/wgxgraphics.cpp \
    Source/Widgets/pdftkguibookmarks.cpp \
    Source/Models/bookmarkslist.cpp \
    Source/Acctions/pdfmetainfo.cpp \
    Source/Threads/getmetainfo.cpp \
    Source/Widgets/bookmarksedit.cpp \
    Source/Threads/metadatawriter.cpp \
    Source/Acctions/pdfmetawriter.cpp \
    Source/Widgets/pagerotationwgx.cpp

HEADERS  += Headers/mainwindow.h \
	Headers/About/aboutdialog.h \
	Headers/Structures/mainwindow.h \
	Headers/Structures/pdfinfo.h \
	Headers/Commons/basefunctions.h \
	Headers/Acctions/crchash.h \
        Headers/Acctions/mimetype.h \
	Headers/Acctions/makeonepdf.h \
	Headers/Acctions/pdfinfo.h \
	Headers/Threads/pdfunion.h \
    Headers/Widgets/pdftkguicat.h \
    Headers/Models/pdflist.h \
    Headers/Threads/getpdfinfo.h \
    Headers/Widgets/addexistfileagain.h \
    Headers/Widgets/pdfwiewform.h \
    Headers/Threads/pdfreaderthreads.h \
    Headers/Structures/common.h \
    Headers/Widgets/windowprogressbar.h \
    Headers/Structures/pdfimagestructures.h \
    Headers/Widgets/wgxgraphics.h \
    Headers/Widgets/pdftkguibookmarks.h \
    Headers/Models/bookmarkslist.h \
    Headers/Acctions/pdfmetainfo.h \
    Headers/Threads/getmetainfo.h \
    Headers/Widgets/bookmarksedit.h \
    Headers/Threads/metadatawriter.h \
    Headers/Acctions/pdfmetawriter.h \
    Headers/Widgets/pagerotationwgx.h

FORMS    += Forms/mainwindow.ui \
	Forms/About/aboutdialog.ui \
    Forms/Widgets/pdftkguicat.ui \
    Forms/Widgets/addexistfileagain.ui \
    Forms/Widgets/pdfwiewform.ui \
    Forms/Widgets/windowprogressbar.ui \
    Forms/Widgets/wgxgraphics.ui \
    Forms/Widgets/pdftkguibookmarks.ui \
    Forms/Widgets/bookmarksedit.ui \
    Forms/Widgets/pagerotationwgx.ui

linux {
LIBS += -lmagic
INCLUDEPATH += /usr/include/poppler/qt5
}
windows {
    INCLUDEPATH += d:/msys64/mingw64/local/include/poppler/qt5
    LIBS += -Ld:/msys64/mingw64/local/lib
}
LIBS += -lpoppler-qt5

TRANSLATIONS = Translations/FreePDFtkGUI-ru_RU.ts

RESOURCES += \
    Resource/icons.qrc

windows {
RC_FILE = Resource/windowsicon.rc
}
