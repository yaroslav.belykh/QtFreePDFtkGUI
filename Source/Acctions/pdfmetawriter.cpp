/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Acctions/pdfmetawriter.h"

PdfMetaWriter::PdfMetaWriter(QSettings *stgns, QTemporaryDir *tDir, QObject *parent) : QObject(parent)
{
    settings=stgns;
    tmpDir=tDir;
}

PdfMetaWriter::~PdfMetaWriter()
{

}

QString PdfMetaWriter::doWriteMeta2Pdf(QString *Data, QString *inputFile, QString *outputFile, int crcAlg)
{
    QString Message;
    QString DataFile;
    QFile dataFile;
    DataFile=tmpDir->path()+QDir::separator()+"data.txt";
    dataFile.setFileName(DataFile);
    if (dataFile.open(QIODevice::WriteOnly))
    {
        dataFile.write(QVariant(*Data).toByteArray());
        dataFile.close();
        QString inFile;
        QString ouFile;
        QProcess Process;
        QStringList params;
        int cnt;
        bool Ok;
        if (*inputFile==*outputFile)
        {
            QFile iFile;
            inFile=tmpDir->path()+QDir::separator()+"ifile.pdf";
            ouFile=tmpDir->path()+QDir::separator()+"ofile.pdf";
            iFile.setFileName(*inputFile);
            iFile.copy(inFile);
        }
        else
        {
            inFile=*inputFile;
            ouFile=*outputFile;
        }
        params << inFile << "update_info_utf8" << DataFile << "output" << ouFile;
        Process.start(settings->value("Path/PDFtk").toString(), params);
        Process.waitForStarted();
        Ok=false;
        cnt=0;
        while (!Ok && cnt<5)
        {
            Ok=Process.waitForFinished(20000);
            cnt++;
        }
        if (Ok)
        {
            QString errorStr;
            errorStr.clear();
            errorStr=QVariant(Process.readAllStandardError()).toString();
            if (errorStr.isEmpty())
            {
                switch(crcAlg)
                {
                    case 0:
                        {Crc = new CrcHash(this, QCryptographicHash::Md5);}
                    break;
                    case 1:
                        {Crc = new CrcHash(this, QCryptographicHash::Sha256);}
                    break;
                    default:
                        {Crc = new CrcHash(this);}
                    break;
                }
                Message = Crc->getCrc(ouFile);
                Crc->deleteLater();
            }
            else
            {
                Message="Error: "+errorStr;
                qDebug() << errorStr;
            }
        }
        else
        {
            qDebug() << "Process is freze";
            if (Process.state()!=QProcess::NotRunning)
            {
                Process.kill();
                Message="Error: timeout aborting";
            }
        }
        dataFile.remove();
    }
    return(Message);
}
