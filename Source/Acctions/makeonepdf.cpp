/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Acctions/makeonepdf.h"

makeOnePdf::makeOnePdf(QSettings *stngs, QObject *parent) : QObject(parent)
{
    settings=stngs;
}

makeOnePdf::~makeOnePdf()
{

}

QString makeOnePdf::MakeOnePDF(QStringList inputFiles, QStringList inputPages, QString fileName, int crcAlg)
{
    QString Message;
    QDir testDir;
    QFile testFile;
    QStringList params;
    QProcess Proc;
    bool Ok;
    int cnt;
    testDir.setPath(fileName.split(fileName.split("/",QString::SkipEmptyParts).last(),QString::SkipEmptyParts).first());
    testFile.setFileName(fileName);
    if (!testDir.exists())
        testDir.mkpath(testDir.path());
    else
    {
        if (testFile.exists())
            {testFile.remove();}
    }
    params << inputFiles << "cat" << inputPages << "output" << fileName;
    qDebug() << settings->value("Path/PDFtk").toString() << params;
    Proc.start(settings->value("Path/PDFtk").toString(), params);
    Proc.waitForStarted();
    Ok=false;
    cnt=0;
    while (!Ok && cnt<5)
    {
        Ok=Proc.waitForFinished(20000);
        cnt++;
    }
    if (Ok)
    {
        QString errorStr;
        errorStr.clear();
        errorStr=QVariant(Proc.readAllStandardError()).toString();
        if (errorStr.isEmpty())
        {
            switch(crcAlg)
            {
                case 0:
                    {Crc = new CrcHash(this, QCryptographicHash::Md5);}
                break;
                case 1:
                    {Crc = new CrcHash(this, QCryptographicHash::Sha256);}
                break;
                default:
                    {Crc = new CrcHash(this);}
                break;
            }
            Message = Crc->getCrc(fileName);
            Crc->deleteLater();
        }
        else
        {
            Message="Error: "+errorStr;
            qDebug() << errorStr;
        }
    }
    else
    {
        qDebug() << "Process is freze";
        if (Proc.state()!=QProcess::NotRunning)
        {
            Proc.kill();
            Message="Error: timeout aborting";
        }
    }
    return(Message);
}
