/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Acctions/pdfmetainfo.h"

PdfMetaInfo::PdfMetaInfo(QSettings *inpSettings, QTemporaryDir *inpTDir, QObject *parent) //: QObject(parent)
{
    settings=inpSettings;
    tmpDir=inpTDir;
}

PdfMetaInfo::~PdfMetaInfo()
{

}

QStringList PdfMetaInfo::getPdfMetaInfo(QString fileName)
{
    QStringList outData;
    if (!fileName.isEmpty())
    {
        QFile filePDF;
        filePDF.setFileName(fileName);
        if (filePDF.exists())
        {
            QStringList params;
            QProcess *Proc;
            QString errorStr;
            bool Ok;
            int k;
            Proc = new QProcess(this);
            params << fileName << "dump_data_utf8";
            qDebug() << tr("Read file")+" "+fileName+".";
            Proc->start(settings->value("Path/PDFtk").toString(), params);
            Proc->waitForStarted();
            Ok=Proc->waitForFinished(30000);
            errorStr=QVariant(Proc->readAllStandardError()).toString();
            if (!errorStr.isEmpty())
                {qDebug() << errorStr;}
            if (Ok && Proc->exitStatus()==QProcess::NormalExit)
                {outData=QVariant(Proc->readAllStandardOutput()).toString().split("\n", QString::SkipEmptyParts);}
            else
                {outData.append("Error in file");}
        }
        else
            {outData.append("Error! File not found!");}
    }
    return(outData);
}

QString PdfMetaInfo::setPdfMetaInfo(QStringList inpData, QString outFileName)
{
    QString messageStr;
    if (inpData.isEmpty())
    {

    }
    return(messageStr);
}
