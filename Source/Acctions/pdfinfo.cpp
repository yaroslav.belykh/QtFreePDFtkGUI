/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Acctions/pdfinfo.h"

PdfInfo::PdfInfo(QSettings *stngs, QObject *parent) : QObject(parent)
{
    settings=stngs;
}

PdfInfo::~PdfInfo()
{

}

TpdfInfoFull PdfInfo::getPDFInfo(QString fileName)
{
    TpdfInfoFull pdfInfo;
    if (!fileName.isEmpty())
    {
        QFile filePDF;
        filePDF.setFileName(fileName);
        if (filePDF.exists())
        {
            QStringList params;
            QProcess *Proc;
            QString errorStr;
            bool Ok;
            int k;
            Proc = new QProcess(this);
            params << fileName << "dump_data_utf8";
            qDebug() << tr("Read file")+" "+fileName+".";
            Proc->start(settings->value("Path/PDFtk").toString(), params);
            Proc->waitForStarted();
            Ok=Proc->waitForFinished(30000);
            errorStr=QVariant(Proc->readAllStandardError()).toString();
            if (!errorStr.isEmpty())
                {qDebug() << errorStr;}
            if (Ok && Proc->exitStatus()==QProcess::NormalExit)
            {
                params.clear();
                params=QVariant(Proc->readAllStandardOutput()).toString().split("\n", QString::SkipEmptyParts);
                k=0;
                for (int i=0;i<params.count();i++)
                {
                    if (params.at(i).indexOf("InfoKey: ModDate")!=-1 && i+1<params.count())
                    {
                        if (params.at(i+1).indexOf("InfoValue: D:")==0 )
                            {pdfInfo.mainInfo.ModDate=QDateTime::fromString(params.at(i+1).split("D:",QString::SkipEmptyParts).last().split("+",QString::SkipEmptyParts).first().split("-",QString::SkipEmptyParts).first(), "yyyyMMddHHmmss");}
                    }
                    if (params.at(i).indexOf("InfoKey: CreationDate")!=-1 && i+1<params.count())
                    {
                        if (params.at(i+1).indexOf("InfoValue: D:")==0 )
                            {pdfInfo.mainInfo.CreationDate=QDateTime::fromString(params.at(i+1).split("D:",QString::SkipEmptyParts).last().split("+",QString::SkipEmptyParts).first().split("-",QString::SkipEmptyParts).first(), "yyyyMMddHHmmss");}
                    }
                    if (params.at(i).indexOf("InfoKey: Author")!=-1 && i+1<params.count())
                    {
                        if (params.at(i+1).indexOf("InfoValue: ")!=-1 )
                            {pdfInfo.mainInfo.Author=params.at(i+1).split("InfoValue: ",QString::SkipEmptyParts).last();}
                    }
                    if (params.at(i).indexOf("InfoKey: Title")!=-1 && i+1<params.count())
                    {
                        if (params.at(i+1).indexOf("InfoValue: ")!=-1 )
                            {pdfInfo.mainInfo.Title=params.at(i+1).split("InfoValue: ",QString::SkipEmptyParts).last();}
                    }
                    if (params.at(i).indexOf("InfoKey: Creator")!=-1 && i+1<params.count())
                    {
                        if (params.at(i+1).indexOf("InfoValue: ")!=-1 )
                            {pdfInfo.mainInfo.Creator=params.at(i+1).split("InfoValue: ",QString::SkipEmptyParts).last();}
                    }
                    if (params.at(i).indexOf("InfoKey: Producer")!=-1 && i+1<params.count())
                    {
                        if (params.at(i+1).indexOf("InfoValue: ")!=-1 )
                            {pdfInfo.mainInfo.Producer=params.at(i+1).split("InfoValue: ",QString::SkipEmptyParts).last();}
                    }
                    if (params.at(i).indexOf("PdfID")==0)
                        {pdfInfo.PdfID[params.at(i).split("PdfID",QString::SkipEmptyParts).last().split(": ",QString::SkipEmptyParts).first().toInt()]=params.at(i).split(": ",QString::SkipEmptyParts).last();}
                    if (params.at(i).indexOf("NumberOfPages:")==0)
                        {pdfInfo.mainInfo.Pages=params.at(i).split("NumberOfPages: ",QString::SkipEmptyParts).last().toInt();}
                    if (params.at(i).indexOf("BookmarkTitle: ")==0)
                    {
                        if (params.at(i).split("BookmarkTitle: ",QString::SkipEmptyParts).count()>0)
                            {pdfInfo.BookMarks[k].Title=params.at(i).split("BookmarkTitle: ",QString::SkipEmptyParts).last();}
                        else
                            {pdfInfo.BookMarks[k].Title="";}
                        if (i+1<params.count())
                        {
                            int m;
                            m=1;
                            while (i+m<params.count() && params.at(i+m).indexOf("BookmarkLevel:")==-1)
                            {
                                pdfInfo.BookMarks[k].Title.append(params.at(i+m));
                                m++;
                            }
                            if (i+m<params.count())
                            {
                                pdfInfo.BookMarks[k].Level=params.at(i+m).split("BookmarkLevel: ",QString::SkipEmptyParts).last().toInt();
                                if (i+m+1<params.count())
                                {
                                    if (params.at(i+m+1).indexOf("BookmarkPageNumber:")==0)
                                        {pdfInfo.BookMarks[k].PageNumber=params.at(i+m+1).split("BookmarkPageNumber: ",QString::SkipEmptyParts).last().toInt();}
                                }
                            }
                        }
                        k++;
                    }
                    if (params.at(i).indexOf("PageMediaNumber:")==0)
                    {
                        if (i+1<params.count())
                        {
                            pdfInfo.PageMedia[params.at(i).split("PageMediaNumber: ",QString::SkipEmptyParts).last().toInt()].Orientation=params.at(i+1).split("PageMediaRotation: ",QString::SkipEmptyParts).last().toInt()/90;
                            if (i+3<params.count())
                            {
                                double dWidth;
                                double dHeigh;
                                dWidth=params.at(i+3).split("PageMediaDimensions: ",QString::SkipEmptyParts).last().split(" ",QString::SkipEmptyParts).first().toDouble();
                                dHeigh=params.at(i+3).split("PageMediaDimensions: ",QString::SkipEmptyParts).last().split(" ",QString::SkipEmptyParts).last().toDouble();
                                pdfInfo.PageMedia[params.at(i).split("PageMediaNumber: ",QString::SkipEmptyParts).last().toInt()].pWidth=int(BaseFunction.roundTo(dWidth*25.4/72));
                                pdfInfo.PageMedia[params.at(i).split("PageMediaNumber: ",QString::SkipEmptyParts).last().toInt()].pHeight=int(BaseFunction.roundTo(dHeigh*25.4/72));
                            }
                        }
                    }
                }
            }
            Proc->deleteLater();
            if (!pdfInfo.mainInfo.ModDate.isValid())
                {pdfInfo.mainInfo.ModDate=QDateTime(QDate(0,0,0), QTime(0,0));}
            if (!pdfInfo.mainInfo.CreationDate.isValid())
                {pdfInfo.mainInfo.CreationDate=QDateTime(QDate(0,0,0), QTime(0,0));}
        }
    }
    return(pdfInfo);
}
