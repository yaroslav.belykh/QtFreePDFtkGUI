/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    settings = new QSettings(QCoreApplication::organizationName(), QCoreApplication::applicationName(), this);

    tmpDir.setAutoRemove(true);

    if (settings->value("Path/PDFtk").toString().isEmpty())
    {
#if defined(Q_OS_WIN)
        settings->setValue("Path/PDFtk", "pdftk.exe");
#else
        settings->setValue("Path/PDFtk", "pdftk");
#endif
        int count;
        QProcess *pdfTK;
        QString cmd;
        QString outStr;
        cmd=settings->value("Path/PDFtk").toString();
        pdfTK = new QProcess();
        count = 0;
        while (outStr.indexOf("Handy Tool for Manipulating PDF Documents")==-1 && count < 5)
        {
            if (count==0)
                {cmd=settings->value("Path/PDFtk").toString();}
            else
            {
                QString BasePath;
                QString Filter;
#if defined(Q_OS_WIN)
                BasePath="C:";
                BasePath.append(QDir::separator());
                BasePath.append("Program files");
                Filter=tr("Executable Files ( *.exe );; All Files ( *.* )");
#elif defined(Q_OS_MAC)
                BasePath="C:"+QDir::separator()+"Program files";
                Filter=tr("All Files ( * )");
#else
                BasePath="/";
                Filter=tr("All Files ( * )");
#endif
                cmd=QFileDialog::getOpenFileName(0, tr("Please chose PdfTk executable file!"), BasePath, Filter);
            }
            pdfTK->start(cmd, QStringList() << "--version");
            pdfTK->waitForStarted();
            pdfTK->waitForFinished();
            outStr=QVariant(pdfTK->readAllStandardOutput()).toString();
            count++;
        }
        if (count==5 && outStr.indexOf("Handy Tool for Manipulating PDF Documents")==-1)
            {doExit();}
        else
            {settings->setValue("Path/PDFtk", cmd);}
    }
    qDebug() << "MainWindow^ pdftk path - "+settings->value("Path/PDFtk").toString();
    if (settings->value("LastDir/Save").toString().isEmpty())
        {settings->setValue("LastDir/Save",QDir::homePath());}
    if (settings->value("LastDir/Open").toString().isEmpty())
        {settings->setValue("LastDir/Open",QDir::homePath());}

    connect(ui->actionNew, SIGNAL(triggered()), this, SLOT(doNew()));
    connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(doExit()));
    connect(ui->actionAbout_Qt, SIGNAL(triggered()), this, SLOT(OpenAboutQt()));

    pdftkFormCreated=false;
    doNew();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::doNew()
{
    if (pdftkFormCreated)
    {
        ui->vlCat->removeWidget(pdfTKcat);
        ui->vlBookmarks->removeWidget(pdfTKbookmarks);
        pdfTKcat->deleteLater();
        pdfTKbookmarks->deleteLater();
    }
    pdfTKcat = new PdfTkGuiCat(settings, ui->tabCat);
    pdfTKbookmarks = new PdfTkGUiBookmarks(settings, &tmpDir, ui->tabBookmarks);
    pdfTKcat->setObjectName(QStringLiteral("pdfCat"));
    pdfTKbookmarks->setObjectName(QStringLiteral("pdfBookMarks"));
    ui->vlCat->addWidget(pdfTKcat);
    ui->vlBookmarks->addWidget(pdfTKbookmarks);
    pdfTKcat->show();
    pdfTKbookmarks->show();
    pdftkFormCreated=true;
}

void MainWindow::doExit()
{
    this->close();
}

void MainWindow::OpenAboutQt()
{
    QMessageBox::aboutQt(this);
}
