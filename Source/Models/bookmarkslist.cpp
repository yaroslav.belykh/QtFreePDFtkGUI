/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Models/bookmarkslist.h"

BookmarksList::BookmarksList(QWidget *parent)
{

}

BookmarksList::~BookmarksList()
{

}

void BookmarksList::updateModel(TpdfBookMarks pdfBookmarksElement, int indexRow, int updateType)
{
    this->beginResetModel();
    switch (updateType)
    {
        case 0:
            {pdfBookmarks[pdfBookmarks.keys().count()]=pdfBookmarksElement;}
        break;
        case 1:
        {
            if (indexRow>=0 && indexRow<=pdfBookmarks.keys().count())
            {
                for (int i=pdfBookmarks.keys().count()-1;i>=indexRow;i--)
                    {pdfBookmarks[i+1]=pdfBookmarks[i];}
                pdfBookmarks[indexRow]=pdfBookmarksElement;
            }
        }
        break;
        case 2:
        {
            if (indexRow>=0 && indexRow<=pdfBookmarks.keys().count())
                {pdfBookmarks[indexRow]=pdfBookmarksElement;}
        }
        break;
        case 3:
        {
            if (indexRow>=0 && indexRow<=pdfBookmarks.keys().count())
            {
                for (int i=indexRow;i<pdfBookmarks.keys().count()-1;i++)
                    {pdfBookmarks[i]=pdfBookmarks[i+1];}
                pdfBookmarks.remove(pdfBookmarks.keys().last());
            }
        }
        break;
        default:
        break;
    }
    this->endResetModel();
}

void BookmarksList::clearData()
{
    this->beginResetModel();
    int rowC;
    rowC=rowCount();
    pdfBookmarks.clear();
    removeRows(0,rowC);
    this->endResetModel();
}

int BookmarksList::rowCount(const QModelIndex &parent) const
{
    return(pdfBookmarks.count());
}

TpdfBookMarks BookmarksList::getPdfMetaData(int index)
{
    return(pdfBookmarks[index]);
}

int BookmarksList::columnCount(const QModelIndex &parent) const
{
    return(4);
}

QVariant BookmarksList::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
            switch (section)
            {
                case 0:
                    {return(tr("№"));}
                break;
                case 1:
                    {return(tr("Pages"));}
                break;
                case 2:
                    {return(tr("Level"));}
                break;
                case 3:
                    {return(tr("Name"));}
                break;
                default:
                    {return(QVariant());}
                break;
            }
    }
    else
        {return(QVariant());}
}

QVariant BookmarksList::data(const QModelIndex &index, int role) const
{
    switch (role)
    {
        case Qt::DisplayRole:
        {
            switch (index.column())
            {
                case 0:
                    {return(QVariant(pdfBookmarks.keys().at(index.row())+1));}
                break;
                case 1:
                    {return(QVariant(pdfBookmarks[index.row()].PageNumber));}
                break;
                case 2:
                    {return(QVariant(pdfBookmarks[index.row()].Level));}
                break;
                case 3:
                    {return(QVariant(pdfBookmarks[index.row()].Title));}
                default:
                break;
            }
        }
        default:
            {return(QVariant());}
    }
}
