/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Models/pdflist.h"

PDFList::PDFList(QWidget *parent)
{
    charID=0;
}

PDFList::~PDFList()
{

}

void PDFList::updateModel(TpdfInfo pdfInfoelement, int indexRow, int updateType)
{
    this->beginResetModel();
    switch (updateType)
    {
        case 0:
            {pdfFiles[indexRow]=pdfInfoelement;}
        break;
        case 1:
        {
            for (int i=indexRow;i<pdfFiles.count()-1;i++)
                {pdfFiles[i]=pdfFiles[i+1];}
            pdfFiles.remove(pdfFiles.lastKey());
        }
        break;
        case 2:
        {
            if (indexRow<pdfFiles.count()-1)
            {
                TpdfInfo tempInfo;
                tempInfo=pdfFiles[indexRow];
                pdfFiles[indexRow]=pdfFiles[indexRow+1];
                pdfFiles[indexRow+1]=tempInfo;
            }
        }
        break;
        case 3:
        {
            if (indexRow>0)
            {
                TpdfInfo tempInfo;
                tempInfo=pdfFiles[indexRow];
                pdfFiles[indexRow]=pdfFiles[indexRow-1];
                pdfFiles[indexRow-1]=tempInfo;
            }
        }
        break;
        case 4:
        {
            if (pdfFiles.keys().contains(indexRow))
                {pdfFiles[pdfFiles.count()]=pdfFiles[indexRow];}
        }
        break;
        case 5:
            {pdfFiles[indexRow]=pdfInfoelement;}
        break;
        default:
        break;
    }
    this->endResetModel();
}

void PDFList::clearData()
{
    this->beginResetModel();
    int rowC;
    rowC=rowCount();
    pdfFiles.clear();
    removeRows(0,rowC);
    this->endResetModel();
}

QStringList PDFList::getAllFiles()
{
    QStringList inputFies;
    for (int i=0;i<pdfFiles.count();i++)
    {
        if (pdfFiles[pdfFiles.keys().at(i)].mimeType.toLower()=="application/pdf")
        {
            bool Ok;
            Ok=true;
            for (int j=0;j<inputFies.count();j++)
            {
                if (inputFies.at(j)==pdfFiles[pdfFiles.keys().at(i)].fileID+"="+pdfFiles[pdfFiles.keys().at(i)].fileName)
                    {Ok=false;}
            }
            if (Ok)
                {inputFies.append(pdfFiles[pdfFiles.keys().at(i)].fileID+"="+pdfFiles[pdfFiles.keys().at(i)].fileName);}
        }
    }
    return(inputFies);
}

QMap<int, QStringList> PDFList::getAllPages()
{
    QMap<int, QStringList> outputPages;
    QStringList inputFies;
    for (int i=0;i<pdfFiles.count();i++)
    {
        if (pdfFiles[pdfFiles.keys().at(i)].mimeType.toLower()=="application/pdf")
        {
            if (pdfFiles[pdfFiles.keys().at(i)].isMakeList)
            {
                QStringList tmpList;
                tmpList=pdfFiles[pdfFiles.keys().at(i)].selectedPages.split(",",QString::SkipEmptyParts);
                for (int j=0;j<tmpList.count();j++)
                    {outputPages[i].append(pdfFiles[pdfFiles.keys().at(i)].fileID+tmpList.at(j));}
            }
            else
                {outputPages[i].append(pdfFiles[pdfFiles.keys().at(i)].fileID);}
        }
    }
    return(outputPages);
}

int PDFList::rowCount(const QModelIndex &parent) const
{
    return(pdfFiles.count());
}

QStringList PDFList::filesList()
{
    QStringList listFile;
    for (int i=0;i<pdfFiles.count();i++)
        {listFile.append(pdfFiles[i].fileName);}
    return listFile;
}

QString PDFList::getFileName(int fileID)
{
    return(pdfFiles[fileID].fileName);
}

QString PDFList::getSelectedPages(int fileID)
{
    return(pdfFiles[fileID].selectedPages);
}

void PDFList::setSelectedPages(int fileID, QString str)
{
    pdfFiles[fileID].selectedPages=str;
}

TpdfInfo PDFList::getCurrentFileINfo(int fileID)
{
    return (pdfFiles[fileID]);
}

int PDFList::columnCount(const QModelIndex &parent) const
{
    return(10);
}

QVariant PDFList::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
            switch (section)
            {
                case 0:
                    {return(tr("№"));}
                break;
                case 1:
                    {return(tr("File name"));}
                break;
                case 2:
                    {return(tr("File type"));}
                break;
                case 3:
                    {return(tr("Pages"));}
                break;
                case 4:
                    {return(tr("Creation date"));}
                break;
                case 5:
                    {return(tr("Title"));}
                break;
                case 6:
                    {return(tr("Author"));}
                break;
                case 7:
                    {return(tr("Creator"));}
                break;
                case 8:
                    {return(tr("Selected Pages"));}
                break;
                case 9:
                    {return(tr("FileID"));}
                break;
                default:
                    {return(QVariant());}
                break;
            }
    }
    else
        {return(QVariant());}
}

QVariant PDFList::data(const QModelIndex &index, int role) const
{
    switch (role)
    {
        case Qt::DisplayRole:
        {
            switch (index.column())
            {
                case 0:
                    {return(QVariant(index.row()+1));}
                break;
                case 1:
                    {return(QVariant(pdfFiles[index.row()].fileName));}
                break;
                case 2:
                    {return(QVariant(pdfFiles[index.row()].mimeType.split("/",QString::SkipEmptyParts).last()));}
                break;
                case 3:
                    {return(QVariant(pdfFiles[index.row()].Pages));}
                break;
                case 4:
                    {return(QVariant(pdfFiles[index.row()].CreationDate.toString("yyyy.MM.dd HH:mm:ss")));}
                break;
                case 5:
                    {return(QVariant(pdfFiles[index.row()].Title));}
                break;
                case 6:
                    {return(QVariant(pdfFiles[index.row()].Author));}
                break;
                case 7:
                   {return(QVariant(pdfFiles[index.row()].Creator));}
                break;
                case 8:
                {
                    if (!pdfFiles[index.row()].selectedPages.isEmpty())
                    {
                        QString dataStr;
                        dataStr=pdfFiles[index.row()].selectedPages;
                        dataStr.replace("east", "Rotate to 90 degree right").replace("west", "Rotate to 90 degree left").replace("south", "Rotate to 180 degree");
                        return(QVariant(dataStr));
                    }
                    else
                        {return(QVariant(tr("All")));}
                }
                break;
                case 9:
                    {return(QVariant(pdfFiles[index.row()].fileID));}
                break;
                default:
                break;
            }
        }
        default:
            {return(QVariant());}
    }
}
