/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Widgets/wgxgraphics.h"
#include "ui_wgxgraphics.h"

wgxGraphics::wgxGraphics(QImage *inpImage, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::wgxGraphics)
{
    ui->setupUi(this);
    this->setParent(parent);
    image=inpImage;
    origImage=0;

    lastImageOrientation=0;

}

wgxGraphics::~wgxGraphics()
{
    delete ui;
}

QSize wgxGraphics::getImageSize()
{
    return(image->size());
}

QImage* wgxGraphics::getImage()
{
    return(image);
}

void wgxGraphics::setOrientation(int orientation)
{
    if (orientation>0 && orientation!=lastImageOrientation)
    {
        if (lastImageOrientation==0)
            {origImage=image;}
        else
            {delete image;}
        image=0;
        QMatrix newTransform;
        if (orientation>3)
            {newTransform.rotate(qreal(orientation));}
        else
            {newTransform.rotate(qreal(orientation*90));}
        image = new QImage(origImage->transformed(newTransform, Qt::SmoothTransformation));
        lastImageOrientation=orientation;
    }
    else
    {
        if (orientation<=0)
        {
            if (origImage!=0)
            {
                delete image;
                image=origImage;
                origImage=0;
            }
        }
    }
}


void wgxGraphics::setScaledSize(int zoom)
{
    this->setMinimumWidth(image->size().width()*zoom/100);
    this->setMinimumHeight(image->size().height()*zoom/100);
}

void wgxGraphics::paintEvent(QPaintEvent *PaintEvent)
{
    QPainter painter(this);

    painter.drawImage(0,0, image->scaled(this->size(),Qt::KeepAspectRatio, Qt::SmoothTransformation));
}
