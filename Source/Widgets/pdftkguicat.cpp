/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Widgets/pdftkguicat.h"
#include "ui_pdftkguicat.h"

PdfTkGuiCat::PdfTkGuiCat(QSettings *inpStngs, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PdfTkGuiCat)
{
    ui->setupUi(this);

    settings=inpStngs;

    pdfListModel = new PDFList(this);
    SortModel = new QSortFilterProxyModel(this);
    SortModel->setSourceModel(pdfListModel);
    ui->tableView->setModel(SortModel);
    ItemSelectionModel = ui->tableView->selectionModel();
    formPDF = 0;
    lastID = -1;

    restore_collumn_width();
}

PdfTkGuiCat::~PdfTkGuiCat()
{
    delete ui;
}

void PdfTkGuiCat::restore_collumn_width()
{
    ui->tableView->setColumnWidth(0, 40);
    ui->tableView->setColumnWidth(1, 480);
    ui->tableView->setColumnWidth(2, 75);
    ui->tableView->setColumnWidth(3, 100);
    ui->tableView->setColumnWidth(4, 150);
    ui->tableView->setColumnWidth(5, 256);
    ui->tableView->setColumnWidth(6, 200);
    ui->tableView->setColumnWidth(7, 256);
    ui->tableView->setColumnWidth(8, 150);
    ui->tableView->hideColumn(9);
    if (pdfListModel->rowCount()>0)
        {ui->pbAddSelected->setEnabled(true);}
    else
        {ui->pbAddSelected->setEnabled(false);}
    if (lastID>=0)
        {ui->tableView->selectRow(lastID);}
}

void PdfTkGuiCat::getExistFile(bool Ok, int curFile)
{
    if (Ok)
    {
        TpdfInfo HIL;
        pdfListModel->updateModel(HIL, curFile, 4);
    }
    getExistFileDiaog->hide();
    disconnect(getExistFileDiaog, SIGNAL(setChooseId(bool,int)), this, SLOT(getExistFile(bool,int)));
    getExistFileDiaog->deleteLater();
    restore_collumn_width();
}

void PdfTkGuiCat::getSelectedPagesList(QString inpStr)
{
    int pdfID;
    TpdfInfo currentInfo;
    pdfID=selectedPDF();
    currentInfo=pdfListModel->getCurrentFileINfo(pdfID);
    currentInfo.selectedPages=inpStr;
    currentInfo.isMakeList=true;
    pdfListModel->updateModel(currentInfo, pdfID, 5);
    restore_collumn_width();
}

void PdfTkGuiCat::currentFileInfo(QString fileName, QString fileMime, int pageCount, QDateTime CreationTime, QString Title, QString Author, QString Creator)
{
    TpdfInfo PdfInfoElement;
    PdfInfoElement.fileName=fileName;
    PdfInfoElement.mimeType=fileMime;
    PdfInfoElement.Pages=pageCount;
    PdfInfoElement.CreationDate=CreationTime;
    PdfInfoElement.Title=Title;
    PdfInfoElement.Author=Author;
    PdfInfoElement.Creator=Creator;
    PdfInfoElement.isMakeList=false;
    if (pdfListModel->charID==0)
    {
        pdfListModel->charID=QChar('A').toLatin1();
        PdfInfoElement.fileID="A";
    }
    else
    {
        PdfInfoElement.fileID=QVariant(QChar(pdfListModel->charID)).toString();
    }
    pdfListModel->charID++;
    pdfListModel->updateModel(PdfInfoElement, pdfListModel->rowCount());
}

void PdfTkGuiCat::allInfoDone()
{
    Thread->quit();
    disconnect(PdfInfo, SIGNAL(currentFileInfo(QString,QString,int,QDateTime,QString,QString,QString)), this, SLOT(currentFileInfo(QString, QString, int, QDateTime, QString, QString, QString)));
    disconnect(PdfInfo, SIGNAL(modelIsUpdated()), this, SLOT(restore_collumn_width()));
    disconnect(Thread, SIGNAL(started()), PdfInfo, SLOT(doWork()));
    disconnect(PdfInfo, SIGNAL(allDone()),Thread, SLOT(quit()));
    disconnect(PdfInfo, SIGNAL(allDone()),this, SLOT(allInfoDone()));
    PdfInfo->deleteLater();
    Thread->deleteLater();
}

void PdfTkGuiCat::pdfUnionDone(QString outFile, int status, QString mesg)
{
    switch (status)
    {
        case 1:
            {QMessageBox::information(this,QObject::tr("File was created"),"MD5 Sum: "+ mesg);}
        break;
        case 2:
            {QMessageBox::critical(this,QObject::tr("File was not fonded!"),mesg);}
        break;
        case 3:
            {QMessageBox::warning(this,QObject::tr("File is not PDF"),mesg);}
        break;
        case 4:
            {QMessageBox::critical(this,QObject::tr("Error!"),tr("Another error!"));}
        break;
        default:
        break;
    }
}

void PdfTkGuiCat::pdfUnionDelete()
{

}

void PdfTkGuiCat::on_pbAdd_clicked()
{
    QStringList strlFile;
    strlFile=QFileDialog::getOpenFileNames(0, tr("Files to add union PDF"), settings->value("LastDir/Open").toString(), tr("PDF Files ( *.pdf );; All Files ( *.* )") );
    if (!strlFile.isEmpty())
    {
        settings->setValue("LastDir/Open", strlFile.at(0).split("/"+strlFile.at(0).split("/",QString::SkipEmptyParts).last(),QString::SkipEmptyParts).first());

        PdfInfo = new getPDFInfo(settings, strlFile);
        Thread = new QThread;

        connect(PdfInfo, SIGNAL(currentFileInfo(QString,QString,int,QDateTime,QString,QString,QString)), this, SLOT(currentFileInfo(QString, QString, int, QDateTime, QString, QString, QString)));
        connect(PdfInfo, SIGNAL(modelIsUpdated()), this, SLOT(restore_collumn_width()));
        connect(Thread, SIGNAL(started()), PdfInfo, SLOT(doWork()));
        connect(PdfInfo, SIGNAL(allDone()),Thread, SLOT(quit()));
        connect(PdfInfo, SIGNAL(allDone()),this, SLOT(allInfoDone()));
        PdfInfo->moveToThread(Thread);
        Thread->start();
    }
}

void PdfTkGuiCat::on_pbChoose_clicked()
{
    QString strFile;
    strFile=QFileDialog::getSaveFileName(0, tr("File to save union PDF"), settings->value("LastDir/Save").toString(), tr("PDF Files ( *.pdf );; All Files ( *.* )") );
    if (!strFile.isEmpty())
    {
        outputFile=strFile;
        settings->setValue("LastDir/Save", strFile.split("/"+strFile.split("/",QString::SkipEmptyParts).last(),QString::SkipEmptyParts).first());
        ui->lineEdit->setText(outputFile);
    }
}

void PdfTkGuiCat::on_pbUp_clicked()
{
    int iList;
    iList=selectedPDF();
    if (iList>0)
    {
        TpdfInfo HIL;
        pdfListModel->updateModel(HIL, iList, 3);
    }
    restore_collumn_width();
}

void PdfTkGuiCat::on_pbDown_clicked()
{
    int iList;
    iList=selectedPDF();
    if (iList<pdfListModel->rowCount()-1)
    {
        TpdfInfo HIL;
        pdfListModel->updateModel(HIL, iList, 2);
    }
    restore_collumn_width();
}

void PdfTkGuiCat::on_pbDelete_clicked()
{
    int iList;
    iList=selectedPDF();
    TpdfInfo HIL;
    pdfListModel->updateModel(HIL, iList, 1);
}

int PdfTkGuiCat::selectedPDF()
{
    int iList;
    QModelIndexList selectedIndexList;
    selectedIndexList = ItemSelectionModel->selectedRows();
    for (int i=0;i<selectedIndexList.count();i++)
    {
        QModelIndex ModelIndex;
        ModelIndex=SortModel->mapToSource(selectedIndexList.at(i));
        iList=ModelIndex.data(Qt::DisplayRole).toInt()-1;
    }
    return(iList);
}

inline void PdfTkGuiCat::deleteViewPDF()
{
    if (formPDF!=0)
    {
        formPDF->hide();
        disconnect(formPDF, SIGNAL(setPages(QString)), this, SLOT(getSelectedPagesList(QString)));
        ui->horizontalLayout->removeWidget(formPDF);
        formPDF->deleteLater();
        formPDF=0;
    }
}

void PdfTkGuiCat::on_pdDoIt_clicked()
{
    QStringList inputFiles;
    QMap<int, QStringList> inputPages;
    inputFiles=pdfListModel->getAllFiles();
    inputPages=pdfListModel->getAllPages();
    if(!outputFile.isEmpty() && !inputFiles.isEmpty())
    {
        TPdfFileList newList;
        newList.inputFiles=inputFiles;
        newList.inputPages=inputPages;
        newList.outputFile=outputFile;
        newList.crcAlg=0;
        qDebug() << "PdfTKGui Cat pdftk path - "+settings->value("Path/PDFtk").toString();
        UnionPDF = new pdfUnion(settings, newList);
        Thread = new QThread;
        connect(UnionPDF, SIGNAL(setUnionPDFStatus(QString,int,QString)), this, SLOT(pdfUnionDone(QString,int,QString)));
        connect(Thread, SIGNAL(started()),UnionPDF, SLOT(doWork()));
        connect(UnionPDF, SIGNAL(finished()), Thread, SLOT(quit()));
        connect(UnionPDF, SIGNAL(finished()), UnionPDF, SLOT(deleteLater()));

        UnionPDF->moveToThread(Thread);
        Thread->start();
    }
}

void PdfTkGuiCat::on_pbAddSelected_clicked()
{
    getExistFileDiaog = new AddExistFileAgain(pdfListModel->filesList());
    getExistFileDiaog->setObjectName(QStringLiteral("ExistFileAdd"));
    connect(getExistFileDiaog, SIGNAL(setChooseId(bool,int)), this, SLOT(getExistFile(bool,int)));
    getExistFileDiaog->show();
}

void PdfTkGuiCat::on_tableView_doubleClicked(const QModelIndex &index)
{
    int pdfID;
    deleteViewPDF();
    pdfID=selectedPDF();
    if (!pdfListModel->getFileName(pdfID).isEmpty())
    {
        formPDF = new PDFWiewForm(settings, pdfListModel->getFileName(pdfID), pdfListModel->getSelectedPages(pdfID), this);
        formPDF->setObjectName(QStringLiteral("MainPDFForm"));
        connect(formPDF, SIGNAL(setPages(QString)), this, SLOT(getSelectedPagesList(QString)));
        ui->horizontalLayout->addWidget(formPDF);
        formPDF->show();
        lastID=pdfID;
    }
}

void PdfTkGuiCat::on_tableView_clicked(const QModelIndex &index)
{
    int pdfID;
    pdfID=selectedPDF();
    if (pdfID!=lastID)
    {
        deleteViewPDF();
        lastID=pdfID;
    }
}
