/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Widgets/pdftkguibookmarks.h"
#include "ui_pdftkguibookmarks.h"

PdfTkGUiBookmarks::PdfTkGUiBookmarks(QSettings *inpSettings, QTemporaryDir *inpTDir, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PdfTkGUiBookmarks)
{
    ui->setupUi(this);

    settings=inpSettings;
    tmpDir=inpTDir;

    bookmarksListModel = new BookmarksList(this);
    SortModel = new QSortFilterProxyModel(this);
    SortModel->setSourceModel(bookmarksListModel);
    ui->tableView->setModel(SortModel);
    ItemSelectionModel = ui->tableView->selectionModel();

     restore_collumn_width();

     PageNumber=0;

     curID=0;

     aData=0;
     LastAPages=0;

     setEnableButtons(false);
}

PdfTkGUiBookmarks::~PdfTkGUiBookmarks()
{
    delete ui;
}

void PdfTkGUiBookmarks::allInfoDone()
{
    Thread->quit();
    disconnect(Thread, SIGNAL(started()), MetaInfo, SLOT(doWork()));
    disconnect(MetaInfo, SIGNAL(allDone()),this, SLOT(allInfoDone()));
    disconnect(MetaInfo, SIGNAL(allDone()), Thread, SLOT(quit()));
    if (!Data.isEmpty() && Data.count()>1)
    {
        for (int i=0;i<Data.count();i++)
        {
            if (Data.at(i).indexOf("NumberOfPages")==0)
            {
                QString tStr;
                tStr=Data.at(i).split("NumberOfPages:",QString::SkipEmptyParts).last();
                if (tStr.at(0)==' ')
                    {tStr.remove(0,1);}
                PageNumber=tStr.toInt();
                bmStringStart=i+1;
            }
            if (Data.at(i)=="BookmarkBegin" && Data.count()>i+3)
            {
                TpdfBookMarks BookmarkElement;
                if (Data.at(i+1).indexOf("BookmarkTitle")==0)
                {
                    BookmarkElement.Title=Data.at(i+1).split("BookmarkTitle:",QString::SkipEmptyParts).last();
                    if (BookmarkElement.Title.at(0)==' ')
                        {BookmarkElement.Title.remove(0,1);}
                }
                if (Data.at(i+2).indexOf("BookmarkLevel")==0)
                {
                    QString tStr;
                    tStr=Data.at(i+2).split("BookmarkLevel:",QString::SkipEmptyParts).last();
                    if (tStr.at(0)==' ')
                        {tStr.remove(0,1);}
                    BookmarkElement.Level=tStr.toInt();
                }
                if (Data.at(i+3).indexOf("BookmarkPageNumber")==0)
                {
                    QString tStr;
                    tStr=Data.at(i+3).split("BookmarkPageNumber:",QString::SkipEmptyParts).last();
                    if (tStr.at(0)==' ')
                        {tStr.remove(0,1);}
                    BookmarkElement.PageNumber=tStr.toInt();
                }
                bookmarksListModel->updateModel(BookmarkElement);
            }
            if (Data.at(i)=="PageMediaBegin")
            {
                bmStringStop=i;
                break;
            }
        }
    }
    Thread->quit();
    restore_collumn_width();
    MetaInfo->deleteLater();
    Thread->deleteLater();
}

void PdfTkGUiBookmarks::allAddInfoDone()
{
    Thread->quit();
    disconnect(Thread, SIGNAL(started()), MetaInfo, SLOT(doWork()));
    disconnect(MetaInfo, SIGNAL(allDone()),this, SLOT(allInfoDone()));
    disconnect(MetaInfo, SIGNAL(allDone()), Thread, SLOT(quit()));
    if (!aData->isEmpty() && aData->count()>1)
    {
        int uPage;
        uPage= LastAPages;
        for (int i=0;i<aData->count();i++)
        {
            if (aData->at(i).indexOf("NumberOfPages")==0)
            {
                QString tStr;
                tStr=aData->at(i).split("NumberOfPages:",QString::SkipEmptyParts).last();
                if (tStr.at(0)==' ')
                    {tStr.remove(0,1);}
                LastAPages=LastAPages+tStr.toInt();
            }
            if (aData->at(i)=="BookmarkBegin" && aData->count()>i+3)
            {
                TpdfBookMarks BookmarkElement;
                if (aData->at(i+1).indexOf("BookmarkTitle")==0)
                {
                    BookmarkElement.Title=aData->at(i+1).split("BookmarkTitle:",QString::SkipEmptyParts).last();
                    if (BookmarkElement.Title.at(0)==' ')
                        {BookmarkElement.Title.remove(0,1);}
                }
                if (aData->at(i+2).indexOf("BookmarkLevel")==0)
                {
                    QString tStr;
                    tStr=aData->at(i+2).split("BookmarkLevel:",QString::SkipEmptyParts).last();
                    if (tStr.at(0)==' ')
                        {tStr.remove(0,1);}
                    BookmarkElement.Level=tStr.toInt();
                }
                if (aData->at(i+3).indexOf("BookmarkPageNumber")==0)
                {
                    QString tStr;
                    tStr=aData->at(i+3).split("BookmarkPageNumber:",QString::SkipEmptyParts).last();
                    if (tStr.at(0)==' ')
                        {tStr.remove(0,1);}
                    BookmarkElement.PageNumber=uPage+tStr.toInt();
                }
                bookmarksListModel->updateModel(BookmarkElement);
            }
            if (aData->at(i)=="PageMediaBegin")
                {break;}
        }
    }
    Thread->quit();
    restore_collumn_width();
    MetaInfo->deleteLater();
    Thread->deleteLater();
    aData->clear();
    delete aData;
    aData=0;
}

void PdfTkGUiBookmarks::restore_collumn_width()
{
    ui->tableView->setColumnWidth(0, 40);
    ui->tableView->setColumnWidth(1, 40);
    ui->tableView->setColumnWidth(2, 40);
    ui->tableView->setColumnWidth(3, 500);
}

void PdfTkGUiBookmarks::editDialogOK(bool Ok, int Page, int Level, QString Title, int typeOp)
{
    EditDialog->hide();
    disconnect(EditDialog, SIGNAL(setData(bool,int,int,QString,int)), this, SLOT(editDialogOK(bool,int,int,QString,int)));
    if (Ok)
    {
        TpdfBookMarks BookmarkElement;
        BookmarkElement.PageNumber=Page;
        BookmarkElement.Level=Level;
        BookmarkElement.Title=Title;
        if (typeOp==0)
        {
            if (curID==0)
                {bookmarksListModel->updateModel(BookmarkElement, 0, 0);}
            else
                {bookmarksListModel->updateModel(BookmarkElement, curID, 1);}
        }
        else
            {bookmarksListModel->updateModel(BookmarkElement, curID, 2);}
    }
    curID=0;
    EditDialog->deleteLater();
    EditDialog=0;
}

void PdfTkGUiBookmarks::getStaus(QString mesg, int status)
{
    switch (status)
    {
        case 1:
            {QMessageBox::information(this,QObject::tr("File was created"),"MD5 Sum: "+ mesg);}
        break;
        case 2:
            {QMessageBox::critical(this,QObject::tr("Error!"),tr("Another error!")+"/n"+mesg);}
        break;
        case 3:
            {QMessageBox::warning(this,QObject::tr("File is not set"),mesg);}
        break;
        default:
        break;
    }
}

void PdfTkGUiBookmarks::writeDone()
{
    Thread->quit();
    disconnect(Thread, SIGNAL(started()), MetaWriter, SLOT(doIt()));
    disconnect(MetaWriter, SIGNAL(allDone()), this, SLOT(writeDone()));
    connect(MetaWriter, SIGNAL(setMessage(QString)), this, SLOT(getStaus(QString)));
    MetaWriter->deleteLater();
    Thread->deleteLater();
}

int PdfTkGUiBookmarks::selectedBookmarks()
{
    int iList;
    iList=-1;
    QModelIndexList selectedIndexList;
    selectedIndexList = ItemSelectionModel->selectedRows();
    for (int i=0;i<selectedIndexList.count();i++)
    {
        QModelIndex ModelIndex;
        ModelIndex=SortModel->mapToSource(selectedIndexList.at(i));
        iList=ModelIndex.data(Qt::DisplayRole).toInt()-1;
    }
    return(iList);
}

void PdfTkGUiBookmarks::on_pbChoose_InPut_clicked()
{
    QString strlFile;
    strlFile=QFileDialog::getOpenFileName(0, tr("Open PDF-file"), settings->value("LastDir/Open").toString(), tr("PDF Files ( *.pdf );; All Files ( *.* )") );
    if (!strlFile.isEmpty())
    {
        bookmarksListModel->clearData();
        settings->setValue("LastDir/Open", strlFile.split("/"+strlFile.split("/",QString::SkipEmptyParts).last(),QString::SkipEmptyParts).first());
        ui->lineEdit_Input->setText(strlFile);
        inputFile=strlFile;

        setEnableButtons(true);

        Data.clear();
        PageNumber=0;
        MetaInfo = new getMetaInfo(settings, tmpDir, &Data, strlFile);
        Thread = new QThread;

        connect(Thread, SIGNAL(started()), MetaInfo, SLOT(doWork()));
        connect(MetaInfo, SIGNAL(allDone()),this, SLOT(allInfoDone()));
        connect(MetaInfo, SIGNAL(allDone()), Thread, SLOT(quit()));

        MetaInfo->moveToThread(Thread);
        Thread->start();
    }
    else
        {setEnableButtons(false);}
}

void PdfTkGUiBookmarks::on_pbChooseOutput_clicked()
{
    QString strFile;
    strFile=QFileDialog::getSaveFileName(0, tr("File to save union PDF"), settings->value("LastDir/Save").toString(), tr("PDF Files ( *.pdf );; All Files ( *.* )") );
    if (!strFile.isEmpty())
    {
        outputFile=strFile;
        settings->setValue("LastDir/Save", strFile.split("/"+strFile.split("/",QString::SkipEmptyParts).last(),QString::SkipEmptyParts).first());
        ui->lineEdit_OutPut->setText(outputFile);
    }
}

void PdfTkGUiBookmarks::on_pbAdd_clicked()
{
    curID=0;
    if (bookmarksListModel->rowCount()==0)
        {EditDialog = new BookMarksEdit(PageNumber, 1);}
    else
    {
        TpdfBookMarks bmb;
        bmb=bookmarksListModel->getPdfMetaData(bookmarksListModel->rowCount()-1);
        EditDialog = new BookMarksEdit(PageNumber, bmb.Level+1, bmb.PageNumber);
    }
    EditDialog->setObjectName(QStringLiteral("PDFBookMarkDialog"));
    EditDialog->setModal(true);
    connect(EditDialog, SIGNAL(setData(bool,int,int,QString,int)), this, SLOT(editDialogOK(bool,int,int,QString,int)));
    EditDialog->show();
}

void PdfTkGUiBookmarks::on_pushButton_clicked()
{
    if (bookmarksListModel->rowCount()>0)
    {
        QString outData;
        for (int i=0;i<bmStringStart;i++)
            {outData.append(Data.at(i)+"\n");}
        for (int i=0;i<bookmarksListModel->rowCount();i++)
        {
            TpdfBookMarks bookmarkElement;
            bookmarkElement=bookmarksListModel->getPdfMetaData(i);
            outData.append("BookmarkBegin\n");
            outData.append("BookmarkTitle: "+bookmarkElement.Title+"\n");
            outData.append("BookmarkLevel: "+QVariant(bookmarkElement.Level).toString()+"\n");
            outData.append("BookmarkPageNumber: "+QVariant(bookmarkElement.PageNumber).toString()+"\n");
        }
        for (int i=bmStringStop;i<Data.count();i++)
        {
            outData.append(Data.at(i)+"\n");
            if (i<Data.count()-1)
                {outData.append("\n");}
        }
        Thread = new QThread;
        MetaWriter = new MetaDataWriter(settings, tmpDir, outData, &inputFile, &outputFile);
        connect(Thread, SIGNAL(started()), MetaWriter, SLOT(doIt()));
        connect(MetaWriter, SIGNAL(allDone()), this, SLOT(writeDone()));
        connect(MetaWriter, SIGNAL(setMessage(QString, int)), this, SLOT(getStaus(QString, int)));
        MetaWriter->moveToThread(Thread);
        Thread->start();
    }
}

void PdfTkGUiBookmarks::on_pbEdit_clicked()
{
    curID=selectedBookmarks();
    if (curID>=0 && curID<bookmarksListModel->rowCount())
    {
        TpdfBookMarks bm;
        int lMax;
        int lMin;
        int pMax;
        int pMin;
        bm=bookmarksListModel->getPdfMetaData(curID);
        if (curID>1)
        {
            TpdfBookMarks bmb;
            bmb=bookmarksListModel->getPdfMetaData(curID-1);
            lMax=bmb.Level+1;
            pMin=bmb.PageNumber;
        }
        else
        {
            lMax=1;
            pMin=1;
        }
        if (curID<bookmarksListModel->rowCount()-1)
        {
            TpdfBookMarks bma;
            bma=bookmarksListModel->getPdfMetaData(curID+1);
            pMax=bma.PageNumber;
            if (bma.Level>1)
                {lMin=bma.Level-1;}
            else
                {lMin=1;}
        }
        else
        {
            pMax=PageNumber;
            lMin=1;
        }
        EditDialog = new BookMarksEdit(pMax, lMax, pMin, lMin, bm.PageNumber, bm.Level, bm.Title, 1);
        EditDialog->setObjectName(QStringLiteral("PDFBookMarkDialog"));
        EditDialog->setModal(true);
        connect(EditDialog, SIGNAL(setData(bool,int,int,QString,int)), this, SLOT(editDialogOK(bool,int,int,QString,int)));
        EditDialog->show();
    }
}

void PdfTkGUiBookmarks::on_pbDelete_clicked()
{
    curID=selectedBookmarks();
    if (curID>=0 && curID<bookmarksListModel->rowCount())
    {
        TpdfBookMarks bm;
        bookmarksListModel->updateModel(bm, curID, 3);
    }
    curID=0;
}

void PdfTkGUiBookmarks::setEnableButtons(bool Status)
{
    ui->pbAdd->setEnabled(Status);
    ui->pbChooseOutput->setEnabled(Status);
    ui->lineEdit_OutPut->setEnabled(Status);
    ui->inpytLbl->setEnabled(Status);
    ui->pushButton->setEnabled(Status);
    ui->pbAddSelected->setEnabled(Status);
    if (!Status)
        {setEnabledEditButtons(Status);}
}

void PdfTkGUiBookmarks::setEnabledEditButtons(bool Status)
{
    ui->pbEdit->setEnabled(Status);
    ui->pbDelete->setEnabled(Status);
    ui->pbInsert->setEnabled(Status);
}

void PdfTkGUiBookmarks::on_tableView_clicked(const QModelIndex &index)
{
    if (selectedBookmarks()>-1)
        {setEnabledEditButtons(true);}
    else
        {setEnabledEditButtons(false);}
}

void PdfTkGUiBookmarks::on_tableView_doubleClicked(const QModelIndex &index)
{
    on_pbEdit_clicked();
}

void PdfTkGUiBookmarks::on_pbInsert_clicked()
{
    curID==selectedBookmarks();
    if (curID>=0 && curID<bookmarksListModel->rowCount())
    {
        int lMax;
        int lMin;
        int pMax;
        int pMin;
        if (curID>1)
        {
            TpdfBookMarks bmb;
            bmb=bookmarksListModel->getPdfMetaData(curID-1);
            lMax=bmb.Level+1;
            pMin=bmb.PageNumber;
        }
        else
        {
            lMax=1;
            pMin=1;
        }
        if (curID<bookmarksListModel->rowCount()-1)
        {
            TpdfBookMarks bma;
            bma=bookmarksListModel->getPdfMetaData(curID+1);
            pMax=bma.PageNumber;
            if (bma.Level>1)
                {lMin=bma.Level-1;}
            else
                {lMin=1;}
        }
        else
        {
            pMax=PageNumber;
            lMin=1;
        }
        EditDialog = new BookMarksEdit(pMax, lMax, pMin, lMin);
        EditDialog->setObjectName(QStringLiteral("PDFBookMarkDialog"));
        EditDialog->setModal(true);
        connect(EditDialog, SIGNAL(setData(bool,int,int,QString,int)), this, SLOT(editDialogOK(bool,int,int,QString,int)));
        EditDialog->show();
    }
}

void PdfTkGUiBookmarks::on_pbAddSelected_clicked()
{
    QString strlFile;
    strlFile=QFileDialog::getOpenFileName(0, tr("Open PDF-file"), settings->value("LastDir/Open").toString(), tr("PDF Files ( *.pdf );; All Files ( *.* )") );
    if (!strlFile.isEmpty())
    {
        settings->setValue("LastDir/Open", strlFile.split(QDir::separator()+strlFile.split(QDir::separator(),QString::SkipEmptyParts).last(),QString::SkipEmptyParts).first());

        aData = new QStringList();
        aData->clear();
        MetaInfo = new getMetaInfo(settings, tmpDir, aData, strlFile);
        Thread = new QThread;

        connect(Thread, SIGNAL(started()), MetaInfo, SLOT(doWork()));
        connect(MetaInfo, SIGNAL(allDone()),this, SLOT(allAddInfoDone()));
        connect(MetaInfo, SIGNAL(allDone()), Thread, SLOT(quit()));

        MetaInfo->moveToThread(Thread);
        Thread->start();
    }
}
