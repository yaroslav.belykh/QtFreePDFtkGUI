/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Widgets/windowprogressbar.h"
#include "ui_windowprogressbar.h"

WindowProgressBar::WindowProgressBar(QString winTitle, QWidget *parent, Qt::WindowModality Modal) :
    QWidget(parent),
    ui(new Ui::WindowProgressBar)
{
    ui->setupUi(this);
    this->setWindowTitle(winTitle);
    this->setWindowModality(Modal);
    ui->progressBar->setValue(0);
}

WindowProgressBar::~WindowProgressBar()
{
    delete ui;
}

void WindowProgressBar::setMaximum(int Maximum)
{
    ui->progressBar->setMaximum(Maximum);
}

void WindowProgressBar::setValue(int Value)
{
    ui->progressBar->setValue(Value);
}

