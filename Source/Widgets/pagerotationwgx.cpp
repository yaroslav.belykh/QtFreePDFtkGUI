/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Widgets/pagerotationwgx.h"
#include "ui_pagerotationwgx.h"

PageRotationWgx::PageRotationWgx(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PageRotationWgx)
{
    ui->setupUi(this);
}

PageRotationWgx::~PageRotationWgx()
{
    delete ui;
}

void PageRotationWgx::enableOrientation(int inpRotation)
{
    Data=inpRotation;
    lastValue=inpRotation;

    switch (inpRotation) {
        case 0:
            {ui->tbOrig->setChecked(true);}
        break;
        case 1:
            {ui->tbEast->setChecked(true);}
        break;
        case 2:
            {ui->tbSouth->setChecked(true);}
        break;
        case 3:
            {ui->tbWest->setChecked(true);}
        break;
        default:
        break;
    }
}

void PageRotationWgx::enableImage(bool stat)
{
    ui->checkBox->setChecked(stat);
}

int PageRotationWgx::Value()
{
    return(Data);
}

bool PageRotationWgx::Status()
{
    return(Checked);
}

void PageRotationWgx::setValue(int button, bool checked)
{
    if (checked)
    {
        bool Ok;
        if (button!=lastValue)
        {
            lastValue=Data;
            Data=button;
            Ok=true;
        }
        else
            {Ok=false;}
        switch (button)
        {
            case 0:
            {
                ui->tbEast->setChecked(false);
                ui->tbSouth->setChecked(false);
                ui->tbWest->setChecked(false);
            }
            break;
            case 1:
            {
                ui->tbOrig->setChecked(false);
                ui->tbSouth->setChecked(false);
                ui->tbWest->setChecked(false);
            }
            break;
            case 2:
            {
                ui->tbOrig->setChecked(false);
                ui->tbEast->setChecked(false);
                ui->tbWest->setChecked(false);
            }
            break;
            case 3:
            {
                ui->tbOrig->setChecked(false);
                ui->tbEast->setChecked(false);
                ui->tbSouth->setChecked(false);
            }
            break;
            default:
            break;
        }
        if (Ok)
            {emit setStatus();}
    }
    else
    {
        if (Data!=lastValue)
        {
            lastValue=Data;
            Data=-1;
            emit setStatus();
        }
    }
}


void PageRotationWgx::on_tbOrig_toggled(bool checked)
{
    setValue(0, checked);
}


void PageRotationWgx::on_tbEast_toggled(bool checked)
{
    setValue(1, checked);
}


void PageRotationWgx::on_tbSouth_toggled(bool checked)
{
    setValue(2, checked);
}

void PageRotationWgx::on_tbWest_toggled(bool checked)
{
    setValue(3, checked);
}

void PageRotationWgx::on_checkBox_toggled(bool checked)
{
    Checked=checked;
    if (checked)
    {
        ui->tbOrig->setEnabled(true);
        ui->tbEast->setEnabled(true);
        ui->tbSouth->setEnabled(true);
        ui->tbWest->setEnabled(true);
    }
    else
    {
        ui->tbOrig->setEnabled(false);
        ui->tbEast->setEnabled(false);
        ui->tbSouth->setEnabled(false);
        ui->tbWest->setEnabled(false);
    }
    emit setStatus();
}
