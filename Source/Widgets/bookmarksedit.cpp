/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Widgets/bookmarksedit.h"
#include "ui_bookmarksedit.h"

BookMarksEdit::BookMarksEdit(int pageMax, int levelMax, int pageMin, int levelMin, int Page, int Level, QString Tittle, int typeDialog, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::BookMarksEdit)
{
    ui->setupUi(this);

    typeOperation=typeDialog;
    switch (typeDialog)
    {
        case 0:
            this->setWindowTitle(tr("Add bookmarks"));
        break;
        case 1:
            this->setWindowTitle(tr("Edit bookmarks"));
        break;
        default:
        break;
    }

    ui->spinBox->setMaximum(pageMax);
    ui->spinBox->setMinimum(pageMin);
    if (Page>=pageMin && Page<=pageMax)
        {ui->spinBox->setValue(Page);}

    for (int i=levelMin;i<=levelMax;i++)
        {ui->comboBox->addItem(QVariant(i).toString());}
    if (Level<=levelMax)
        {ui->comboBox->setCurrentText(QVariant(Level).toString());}

    ui->lineEdit->setText(Tittle);
}

BookMarksEdit::~BookMarksEdit()
{
    delete ui;
}

void BookMarksEdit::on_buttonBox_clicked(QAbstractButton *button)
{
    if (ui->buttonBox->standardButton(button)==QDialogButtonBox::Ok)
        {emit setData(true, ui->spinBox->value(), ui->comboBox->currentText().toInt(), ui->lineEdit->text(), typeOperation);}
    else
        {emit setData(false, 0, 0, "", typeOperation);}
}
