/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Widgets/pdfwiewform.h"
#include "ui_pdfwiewform.h"

PDFWiewForm::PDFWiewForm(QSettings *inpSettings, QString inpFilePath, QString inpSelectedPages, QWidget *parent, int dpiV, int dpiP) :
    QWidget(parent),
    ui(new Ui::PDFWiewForm)
{
    ui->setupUi(this);


    this->setParent(parent);
    this->setLocale(QLocale::system());

    settings=inpSettings;
    ipf=inpFilePath;
    printDPI=dpiP;
    viewDPI=dpiV;
    intSelectedPages=makeSelectedPagesList(inpSelectedPages);
    intRotationPage=makeRotationPageList(inpSelectedPages);

    ui->scrollArea->setAlignment(Qt::AlignHCenter);
    ui->scrollVL->setAlignment(Qt::AlignCenter);

    ReaderPDF = new PdfReaderThreads(&ipf, viewDPI);
    ProgressBar = new WindowProgressBar(tr("Opening PDF file"));
    Thread = new QThread;

    connect(ReaderPDF, SIGNAL(setPage(int,QImage,QSize,int)), this, SLOT(getPage(int,QImage,QSize,int)));
    connect(ReaderPDF, SIGNAL(getPageCount(int)), this, SLOT(getPageCount(int)));
    connect(Thread, SIGNAL(started()), ReaderPDF, SLOT(doPdf2ImageRender()));
    connect(ReaderPDF, SIGNAL(allDone()), Thread, SLOT(quit()));
    connect(ReaderPDF, SIGNAL(allDone()), this, SLOT(openedEnd()));

    ReaderPDF->moveToThread(Thread);
    ProgressBar->show();
    Thread->start();
}

PDFWiewForm::~PDFWiewForm()
{
    while (GraphicsWGX.keys().count()>0)
    {
        int k;
        k=GraphicsWGX.keys().at(0);
        ui->scrollVL->removeWidget(GraphicsWGX[k]);
        if (mLabel.keys().count()>0)
            {ui->scrollVL->removeWidget(mLabel[mLabel.keys().at(0)]);}
        if (mPageRotation.keys().count()>0)
        {
            ui->scrollVL->removeWidget(mPageRotation[mPageRotation.keys().at(0)]);
            disconnect(mPageRotation[mPageRotation.keys().at(0)], SIGNAL(setStatus()), this, SLOT(rechoosePages()));
            mPageRotation[mPageRotation.keys().at(0)]->deleteLater();
        }
        delete GraphicsWGX[k];
        GraphicsWGX.remove(k);
        delete mLabel[k];
        mLabel.remove(k);
        mPageRotation.remove(k);
    }
    delete ui;
}

void PDFWiewForm::setRotate(int imageIndex, int Orientatiom)
{
    if (imageIndex>=0 && imageIndex<mImage.count())
    {
        GraphicsWGX[imageIndex]->setOrientation(Orientatiom);
    }
}

void PDFWiewForm::setScaleChange(QString inpScale)
{
    for (int i=0;i<GraphicsWGX.keys().count();i++)
        {GraphicsWGX[GraphicsWGX.keys().at(i)]->setScaledSize(inpScale.toInt());}
}

void PDFWiewForm::getPage(int pageNumber, QImage inpImage, QSize pageSize, int Orient)
{
    mImage[pageNumber].Image=inpImage;
    mImage[pageNumber].Size=pageSize;
    mImage[pageNumber].Orientation=Orient;
    GraphicsWGX[pageNumber] = new wgxGraphics(&mImage[pageNumber].Image,ui->scrollArea);
    GraphicsWGX[pageNumber]->setOrientation(mImage[pageNumber].Orientation-1);
    mLabel[pageNumber] = new QLabel(QVariant(pageNumber+1).toString(),ui->scrollArea);
    mLabel[pageNumber]->setAlignment(Qt::AlignHCenter);
    mPageRotation[pageNumber] = new PageRotationWgx(ui->scrollArea);
    mPageRotation[pageNumber]->enableImage(checkSelectedList(pageNumber+1));
    mPageRotation[pageNumber]->enableOrientation(orientationSelectList(pageNumber+1));
    mPageRotation[pageNumber]->setObjectName(QStringLiteral("OrientationBox_")+QVariant(pageNumber).toString());
    GraphicsWGX[pageNumber]->setObjectName(QStringLiteral("image_")+QVariant(pageNumber).toString());
    connect(mPageRotation[pageNumber], SIGNAL(setStatus()), this, SLOT(rechoosePages()));
    ui->scrollVL->insertWidget(pageNumber*3, GraphicsWGX[pageNumber]);
    ui->scrollVL->insertWidget(pageNumber*3+1,mLabel[pageNumber]);
    ui->scrollVL->insertWidget(pageNumber*3+2, mPageRotation[pageNumber]);
    setScaleChange("20");
    ProgressBar->setValue(pageNumber+1);
}

void PDFWiewForm::getPageCount(int pCount)
{
    ProgressBar->setMaximum(pCount);
}

void PDFWiewForm::getPageProgreeNumber(int)
{

}

void PDFWiewForm::getPagePrepareComplete(bool, int)
{

}

void PDFWiewForm::openedEnd()
{
    disconnect(ReaderPDF, SIGNAL(setPage(int,QImage,QSize,int)), this, SLOT(getPage(int,QImage,QSize,int)));
    disconnect(ReaderPDF, SIGNAL(getPageCount(int)), this, SLOT(getPageCount(int)));
    disconnect(Thread, SIGNAL(started()), ReaderPDF, SLOT(doPdf2ImageRender()));
    disconnect(ReaderPDF, SIGNAL(allDone()), Thread, SLOT(quit()));
    disconnect(ReaderPDF, SIGNAL(allDone()), this, SLOT(openedEnd()));
    ProgressBar->hide();
    ReaderPDF->deleteLater();
    Thread->deleteLater();
    ProgressBar->deleteLater();
    ReaderPDF=0;
    Thread=0;
}

void PDFWiewForm::rechoosePages()
{
    QString SelectedLists;
    SelectedLists.clear();
    bool OK;
    bool Ok;
    Ok=true;
    OK=true;
    for (int i=0;i<mPageRotation.keys().count();i++)
    {
        int dOri;
        dOri=mPageRotation[mPageRotation.keys().at(i)]->Value();
        GraphicsWGX[GraphicsWGX.keys().at(i)]->setOrientation(dOri);
        if (mPageRotation[mPageRotation.keys().at(i)]->Status())
        {
            if (!SelectedLists.isEmpty())
                {SelectedLists.append(",");}
            if (dOri>0)
            {
                Ok=false;
                QString str;
                str=QVariant(mPageRotation.keys().at(i)+1).toString();
                switch (dOri)
                {
                    case 1:
                        {str.append("east");}
                    break;
                    case 2:
                        {str.append("south");}
                    break;
                    case 3:
                        {str.append("west");}
                    break;
                    default:
                    break;
                }
                SelectedLists.append(str);
            }
            else
                {SelectedLists.append(QVariant(mPageRotation.keys().at(i)+1).toString());}
        }
        else
            {OK=false;}
    }
    if (Ok && OK)
        {SelectedLists.clear();}
    emit setPages(SelectedLists);
}

QList<int> PDFWiewForm::makeSelectedPagesList(QString inpStr)
{
    QList<int> outList;
    QStringList tmpList;
    tmpList=inpStr.split(",",QString::SkipEmptyParts);
    for (int i=0;i<tmpList.count();i++)
    {
        if (tmpList.at(i).contains("east") || tmpList.at(i).contains("south") || tmpList.at(i).contains("west") || tmpList.at(i).contains("north") ||
                tmpList.at(i).contains("left") || tmpList.at(i).contains("right") || tmpList.at(i).contains("down"))
        {
            QString tmpS;
            tmpS=tmpList.at(i);
            if (tmpS.contains("east"))
                {tmpS.remove(tmpS.indexOf("east"),4);}
            if (tmpS.contains("south"))
                {tmpS.remove(tmpS.indexOf("south"),5);}
            if (tmpS.contains("west"))
                {tmpS.remove(tmpS.indexOf("west"),4);}
            if (tmpS.contains("north"))
                {tmpS.remove(tmpS.indexOf("north"),5);}
            if (tmpS.contains("left"))
                {tmpS.remove(tmpS.indexOf("left"),4);}
            if (tmpS.contains("right"))
                {tmpS.remove(tmpS.indexOf("right"),5);}
            if (tmpS.contains("down"))
                {tmpS.remove(tmpS.indexOf("down"),4);}
            outList.append(tmpS.toInt());
        }
        else
            {outList.append(tmpList.at(i).toInt());}
    }
    return(outList);
}

QMap<int, int> PDFWiewForm::makeRotationPageList(QString inpStr)
{
    QMap<int, int> outList;
    QStringList tmpList;
    tmpList=inpStr.split(",",QString::SkipEmptyParts);
    for (int i=0;i<tmpList.count();i++)
    {
        if (tmpList.at(i).contains("east") || tmpList.at(i).contains("south") || tmpList.at(i).contains("west") || tmpList.at(i).contains("north") ||
                tmpList.at(i).contains("left") || tmpList.at(i).contains("right") || tmpList.at(i).contains("down"))
        {
            QString tmpS;
            tmpS=tmpList.at(i);
            if (tmpS.contains("east"))
             {
                tmpS.remove(tmpS.indexOf("east"),4);
                outList[tmpS.toInt()]=1;
            }
            if (tmpS.contains("south"))
            {
                tmpS.remove(tmpS.indexOf("south"),5);
                outList[tmpS.toInt()]=2;
            }
            if (tmpS.contains("west"))
            {
                tmpS.remove(tmpS.indexOf("west"),4);
                outList[tmpS.toInt()]=3;
            }
        }
    }
    return(outList);
}

bool PDFWiewForm::checkSelectedList(int lPage)
{
    if (intSelectedPages.isEmpty() || intSelectedPages.indexOf(lPage)!=-1)
        {return(true);}
    else
        {return(false);}
}

int PDFWiewForm::orientationSelectList(int lPage)
{
    if (!intRotationPage.keys().isEmpty() && intRotationPage.keys().contains(lPage))
        {return(intRotationPage[lPage]);}
    else
    {
        if (mImage.keys().isEmpty() && mImage.keys().contains(lPage))
        {
            if (mImage[lPage].Orientation>3)
                {return(mImage[lPage].Orientation/90);}
            else
                {return(mImage[lPage].Orientation);}
        }
        else
            {return(-1);}
    }
}
