/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Threads/pdfreaderthreads.h"

PdfReaderThreads::PdfReaderThreads(QString *inpPDF, int inpDPI, QObject *parent) : QObject(parent)
{
    pdfFilePath=inpPDF;
    dpi=inpDPI;
}

int PdfReaderThreads::getPageNumbers()
{
    int pageNumbers;
    pdfFile = Poppler::Document::load(*pdfFilePath);
    pageNumbers=pdfFile->numPages();
    delete pdfFile;
    return(pageNumbers);
}

void PdfReaderThreads::doPdf2ImageRender()
{
    pdfFile = Poppler::Document::load(*pdfFilePath);
    emit getPageCount(pdfFile->numPages());
    for (int i=0;i<pdfFile->numPages();i++)
    {
        QImage mImage;
        QSize mSize;
        Poppler::Page::Orientation mOri;
        mImage=pdfFile->page(i)->renderToImage(dpi,dpi);
        mSize=pdfFile->page(i)->pageSize();
        mOri=pdfFile->page(i)->orientation();
        emit setPage(i, mImage, mSize, int(mOri));
    }
    delete pdfFile;
    emit allDone();
}
