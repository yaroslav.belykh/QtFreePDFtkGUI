/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Threads/metadatawriter.h"

MetaDataWriter::MetaDataWriter(QSettings *stgns, QTemporaryDir *tDir, QString inpData, QString *inFile, QString *ouFile, QObject *parent) : QObject(parent)
{
    settings=stgns;
    tmpDir=tDir;
    Data = inpData;
    inpFile=inFile;
    outFile=ouFile;
}

MetaDataWriter::~MetaDataWriter()
{

}

void MetaDataWriter::doIt()
{
    QString Message;
    if (!inpFile->isEmpty() && !outFile->isEmpty())
    {
        WriterPdf = new PdfMetaWriter(settings, tmpDir, this);
        Message=WriterPdf->doWriteMeta2Pdf(&Data, inpFile, outFile, 0);
        WriterPdf->deleteLater();
        if (Message.indexOf("Error:")==-1)
            {emit setMessage(Message, 1);}
        else
            {emit setMessage(Message, 2);}
    }
    else
        {emit setMessage("Input or output file is not set.", 3);}
    emit allDone();
}
