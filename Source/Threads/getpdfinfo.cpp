/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Threads/getpdfinfo.h"

getPDFInfo::getPDFInfo(QSettings *stgs, QStringList pdfList, QObject *parent) : QObject(parent)
{
    settings=stgs;
    pdfFiles=pdfList;
}

void getPDFInfo::doWork()
{
    infoPDF = new PdfInfo(settings, this);
    MimeType = new mimeType;
    for (int i=0;i<pdfFiles.count();i++)
    {
        TpdfInfoFull infoFile;
        infoFile=infoPDF->getPDFInfo(pdfFiles.at(i));
        emit currentFileInfo(pdfFiles.at(i), MimeType->getMimeType(pdfFiles.at(i)), infoFile.mainInfo.Pages, infoFile.mainInfo.CreationDate, infoFile.mainInfo.Title, infoFile.mainInfo.Author, infoFile.mainInfo.Creator);
        emit modelIsUpdated();
    }
    infoPDF->deleteLater();
    MimeType->deleteLater();
    emit allDone();
}
