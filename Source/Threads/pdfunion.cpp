/*********************************************************************
Copyright 2018-2021 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/Threads/pdfunion.h"

pdfUnion::pdfUnion(QSettings *stngs, TPdfFileList wl, QObject *parent) : QObject(parent)
{
    settings=stngs;
    workList=wl;
    connect(this, SIGNAL(finished()), this, SLOT(deleteLater()));
}

pdfUnion::~pdfUnion()
{

}

void pdfUnion::doWork()
{
    bool Ok;
    QStringList inputFiles;
    QStringList inputPages;
    QString messageStr;
    Ok=true;
    if (workList.inputFiles.count()!=0)
    {
        qDebug() << "Cut PDF "+workList.outputFile;
        for (int j=0;j<workList.inputFiles.count();j++)
        {
            QFile testFile;
            testFile.setFileName(getFilePath((workList.inputFiles.at(j))));
            if (testFile.exists() && Ok)
            {
                inputFiles.append(workList.inputFiles.at(j));
            }
            else
            {
                if (!testFile.exists())
                {
                    bool tempOk;
                    tempOk=false;
                    for (int k=0;k<settings->value("MayNotExistPDF").toString().split("||",QString::SkipEmptyParts).count();k++)
                    {
                        if (workList.inputFiles.at(j).indexOf(settings->value("MayNotExistPDF").toString().split("||",QString::SkipEmptyParts).at(k))!=-1)
                            {tempOk=true;}
                    }
                    if (!tempOk)
                    {
                        messageStr.append(workList.inputFiles.at(j)+" ");
                        Ok=false;
                    }
                }
            }
        }
        for (int j=0;j<workList.inputPages.count();j++)
        {
            bool OOK;
            OOK=false;
            for (int k=0;k<inputFiles.count();k++)
            {
                if (workList.inputPages[j].count()>0)
                {
                    qDebug () << workList.inputPages[j].at(0).at(0) << inputFiles.at(k).at(0);
                    if (inputFiles.at(k).at(0)==workList.inputPages[j].at(0).at(0))
                        {OOK=true;}
                }
            }
            if (OOK)
                {inputPages.append(workList.inputPages[j]);}
        }
        if (Ok)
        {
            MimeType = new mimeType(this);
            for (int j=0;j<inputFiles.count();j++)
            {
                if (MimeType->getMimeType(getFilePath(inputFiles.at(j)))!="application/pdf")
                {
                    messageStr.append(inputFiles.at(j)+" ");
                    Ok=false;
                }
            }
            MimeType->deleteLater();
            if (Ok)
            {
                makeOnePdf *pdfMaker;
                pdfMaker = new makeOnePdf(settings, this);
                qDebug() << "PdfTK Union pdftk path - "+settings->value("Path/PDFtk").toString();
                messageStr=pdfMaker->MakeOnePDF(inputFiles, inputPages, workList.outputFile, workList.crcAlg);
                if (messageStr.indexOf("Error:")==-1)
                    {emit setUnionPDFStatus(workList.outputFile, 1, messageStr);}
                else
                    {emit setUnionPDFStatus(workList.outputFile, 4, messageStr);}
                pdfMaker->deleteLater();
            }
            else
                {emit setUnionPDFStatus(workList.outputFile, 3, tr("MIME type is not PDF: ")+messageStr);}
        }
        else
            {emit setUnionPDFStatus(workList.outputFile, 2, tr("File not founded: ")+messageStr);}
    }
    else
    {
        if (!workList.outputFile.isEmpty())
        {
            qDebug() << "Make CRC: "+workList.outputFile;
            QFile testFile;
            testFile.setFileName(settings->value("ProjectPath").toString()+QDir::separator()+workList.outputFile);
            if (testFile.exists())
            {
                switch(workList.crcAlg)
                {
                    case 0:
                        {Crc = new CrcHash(this, QCryptographicHash::Md5);}
                    break;
                    case 1:
                        {Crc = new CrcHash(this, QCryptographicHash::Sha256);}
                    break;
                    default:
                       {Crc = new CrcHash(this);}
                    break;
                }
                messageStr = Crc->getCrc(settings->value("ProjectPath").toString()+QDir::separator()+workList.outputFile);
                Crc->deleteLater();
                emit setUnionPDFStatus(workList.outputFile, 1, messageStr);
            }
            else
                {emit setUnionPDFStatus(workList.outputFile, 2, tr("File not founded: ")+workList.outputFile);}
        }
    }
    emit finished();
}

QString pdfUnion::getFilePath(QString inpStr)
{
    QString outStr;
    int eqindex;
    eqindex=inpStr.indexOf("=");
    inpStr.remove(0,eqindex+1);
    return(inpStr);
}
