/*********************************************************************
Copyright 2018 Yaroslav Belykh (aka Yamah)

This file is part of FreePDFtkGUI

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/


#include "Headers/mainwindow.h"
#include <QApplication>
#include <QProcessEnvironment>
#include <QTranslator>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QCoreApplication::setApplicationName("FreePDFtkGUI");
    QCoreApplication::setOrganizationName("Y.Belykh");
    QCoreApplication::setOrganizationDomain("team.belykh.me");

    QTranslator translator;
    TInputParams inputparams;
#if defined(Q_OS_LINUX)
    QProcessEnvironment *qpe;
    qpe = new QProcessEnvironment();
    *qpe = QProcessEnvironment::systemEnvironment();

    inputparams.ProgramConfPath="/etc/FreePDFtkGUI";
    if (QCoreApplication::applicationDirPath()=="/usr/bin")
    {
        inputparams.ProgramWworkPath="/usr/share/QProjectlient";
        inputparams.ProgramLibPath=qpe->value("LD_LIBRARY_PATH")+QDir::separator()+QCoreApplication::applicationName();
    }
    else
    {
        if (QCoreApplication::applicationDirPath()=="/usr/local/bin")
        {
            inputparams.ProgramWworkPath="/usr/local/share/"+QCoreApplication::applicationName();
            inputparams.ProgramLibPath="/usr/local/"+qpe->value("LD_LIBRARY_PATH").split("/usr/",QString::SkipEmptyParts).last()+"/"+QCoreApplication::applicationName();
        }
        else
        {
#endif
        inputparams.ProgramWworkPath="../QtFreePDFtkGUI";
        inputparams.ProgramLibPath="../QtFreePDFtkGUI";
        inputparams.ProgramConfPath="../QtFreePDFtkGUI";
#if defined(Q_OS_LINUX)
        }
    }
    delete qpe;
    qpe=0;
#endif
    bool OK;
    OK=translator.load(inputparams.ProgramWworkPath+"/Translations/"+QCoreApplication::applicationName()+"-"+QLocale::system().name());
    if (OK)
        {a.installTranslator(&translator);}
    else
        {qDebug() << "Error load Translation "+QLocale::system().name()+"\n"+inputparams.ProgramWworkPath+"/Translations/"+QCoreApplication::applicationName()+"-"+QLocale::system().name();}


    MainWindow w;
    w.show();

    return a.exec();
}
